var deps = ["tools.table"];
include("tools.tests_table", deps, function(Table) {
	module('Tools - Table',{
		setup: function() {
		}
	});

	QUnit.test("at & resize - test 1", function( assert ) {
		var t = new Table();

		assert.ok(!t.at(1,1), "no row, no col");
		assert.ok(!t.at(1,2));
		assert.ok(!t.at(2,1));
		assert.ok(!t.at(2,2));

		t.resize(1,1);
		assert.ok(t.at(1,1), "one row, one col");
		assert.ok(!t.at(1,2));
		assert.ok(!t.at(2,1));
		assert.ok(!t.at(2,2));

		t.resize(1,2);
		assert.ok(t.at(1,1), "two rows, one col");
		assert.ok(t.at(1,2));
		assert.ok(!t.at(2,1));
		assert.ok(!t.at(2,2));

		t.resize(2,1);
		assert.ok(t.at(1,1), "one row, two cols");
		assert.ok(!t.at(1,2));
		assert.ok(t.at(2,1));
		assert.ok(!t.at(2,2));

		t.resize(2,2);
		assert.ok(t.at(1,1), "two rows, two cols");
		assert.ok(t.at(1,2));
		assert.ok(t.at(2,1));
		assert.ok(t.at(2,2));
	});

	QUnit.test("at & last", function( assert ) {
		var t = new Table(function(data) {
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.propEqual(t.at(1, "last").data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2, "last").data, {name:"2,2",x:2,y:2});

		assert.propEqual(t.at("last", 1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at("last", 2).data, {name:"2,2",x:2,y:2});

		assert.propEqual(t.at("last", "last").data, {name:"2,2",x:2,y:2});
	});


	QUnit.test("set_data 1", function( assert ) {
		var t = new Table();

		t.resize(1,1);

		t.at(1,1).data = "test 1"

		assert.ok(t.at(1,1).data, "test 1");
	});

	QUnit.test("set_data 2", function( assert ) {
		var t = new Table();

		t.resize(1,1);

		var data = {name:"toto"};

		t.at(1,1).data = data

		assert.equal(t.at(1,1).data.name, "toto");

		data.name = "titi"
		assert.equal(t.at(1,1).data.name, "titi");

		t.at(1,1).data.name = "tutu"
		assert.equal(data.name, "tutu");
	});

	QUnit.test("insert data", function( assert ) {
		var t = new Table();

		t.resize(1,1);
		assert.ok(t.at(1,1), "one row, one col");
		assert.ok(!t.at(1,2));
		assert.ok(!t.at(2,1));
		assert.ok(!t.at(2,1));

		t.at(1,1).data = "data 1,1";
		assert.equal(t.at(1,1).data, "data 1,1", "assign data works");

		t.insert("test", 1,1, "right", true);
		assert.equal(t.row_count, 1, "insert data 1,1 and move toward right");
		assert.equal(t.col_count, 2);
		assert.ok(t.at(1,1));
		assert.ok(t.at(2,1));
		assert.ok(!t.at(1,2));
		assert.ok(!t.at(2,2));

		assert.equal(t.at(1,1).data, "test", "data has moved to 1,2");
		assert.equal(t.at(2,1).data, "data 1,1");

		t.insert("test 2",2,1,"down", true);
		assert.ok(t.at(1,1), "insert data 1,2 and move toward bottom");
		assert.ok(t.at(1,2));
		assert.ok(t.at(2,1));
		assert.ok(t.at(2,1));

		assert.equal(t.at(1,1).data, "test");
		assert.equal(t.at(1,2).data, null);
		assert.equal(t.at(2,1).data, "test 2");
		assert.equal(t.at(2,2).data, "data 1,1");
	});

	QUnit.test("insert data 2", function( assert ) {
		var t = new Table();

		assert.equal(t.row_count, 0,"no data");
		assert.equal(t.col_count, 0);

		t.resize(1,1);
		t.insert("test", 1,1, "right", true);
		assert.equal(t.row_count, 1, "insert data 1,1 and move toward right");
		assert.equal(t.col_count, 2);
		assert.equal(t.at(1,1).data, "test");

		assert.ok(t.at(1,1));
		assert.ok(t.at(2,1));
		assert.ok(!t.at(1,2));
		assert.ok(!t.at(2,2));
	});

	QUnit.test("insert data 3", function( assert ) {
		var t = new Table();

		t.resize(2,2);

		t.at(1,1).data = "1,1";
		t.at(1,2).data = "1,2";
		t.at(2,1).data = "2,1";
		t.at(2,2).data = "2,2";

		t.insert("test1", 1,1, "right", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test1");
		assert.equal(t.at(2,1).data, "1,1-2,1");
		assert.equal(t.at(3,1).data, "2,1-3,1");
		assert.equal(t.at(1,2).data, "1,2");
		assert.equal(t.at(2,2).data, "2,2");
		assert.equal(t.at(3,2).data, null);

		t.insert("test2",1,1, "right", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test2");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "1,1-2,1-3,1");
		assert.equal(t.at(1,2).data, "1,2");
		assert.equal(t.at(2,2).data, "2,2");
		assert.equal(t.at(3,2).data, null);

		t.insert("test3",1,1, "down", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test3");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "1,1-2,1-3,1");
		assert.equal(t.at(1,2).data, "test2-1,2");
		assert.equal(t.at(2,2).data, "2,2");
		assert.equal(t.at(3,2).data, null);
		assert.equal(t.at(1,3).data, "1,2-1,3");
		assert.equal(t.at(2,3).data, null);
		assert.equal(t.at(3,3).data, null);



		t.insert("test4",1,1, "down", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test4");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "1,1-2,1-3,1");
		assert.equal(t.at(1,2).data, "test3-1,2");
		assert.equal(t.at(2,2).data, "2,2");
		assert.equal(t.at(3,2).data, null);
		assert.equal(t.at(1,3).data, "test2-1,2-1,3");
		assert.equal(t.at(2,3).data, null);
		assert.equal(t.at(3,3).data, null);
		assert.equal(t.at(1,4).data, "1,2-1,3-1,4");
		assert.equal(t.at(2,4).data, null);
		assert.equal(t.at(3,4).data, null);
	});

	QUnit.test("insert data 3", function( assert ) {
		var t = new Table(function(data){
			data.data = data.x +","+data.y;
		});

		assert.throws(function() {
				t.insert("t1",1,1,"down",false)
			},"1,1 is not in the range","");

		assert.ok(!t.at(1,1));

		assert.throws(function() {
				t.insert("t1",1,1,"right",false)
			},"1,1 is not in the range","");

		assert.ok(!t.at(1,1));

		t.resize(1,1);
		t.insert("t1",1,1,"down",true);
		assert.ok(t.at(1,1));
		assert.ok(!t.at(2,1));
		assert.ok(t.at(1,2));
		assert.ok(!t.at(2,2));
	})

	QUnit.test("default data", function( assert ) {
		var t = new Table(function(data) {
			data.data = "empty(" + data.x + "," + data.y + ")";
		});

		t.resize(2,2);

		assert.equal(t.at(1,1).data, "empty(1,1)");
		assert.equal(t.at(1,2).data, "empty(1,2)");
		assert.equal(t.at(2,1).data, "empty(2,1)");
		assert.equal(t.at(2,2).data, "empty(2,2)");

		t.insert("test1", 1,1, "right", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test1");
		assert.equal(t.at(2,1).data, "empty(1,1)-2,1");
		assert.equal(t.at(3,1).data, "empty(2,1)-3,1");
		assert.equal(t.at(1,2).data, "empty(1,2)");
		assert.equal(t.at(2,2).data, "empty(2,2)");
		assert.equal(t.at(3,2).data, "empty(3,2)");

		t.insert("test2",1,1, "right", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test2");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "empty(1,1)-2,1-3,1");
		assert.equal(t.at(1,2).data, "empty(1,2)");
		assert.equal(t.at(2,2).data, "empty(2,2)");
		assert.equal(t.at(3,2).data, "empty(3,2)");

		t.insert("test3",1,1, "down", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test3");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "empty(1,1)-2,1-3,1");
		assert.equal(t.at(1,2).data, "test2-1,2");
		assert.equal(t.at(2,2).data, "empty(2,2)");
		assert.equal(t.at(3,2).data, "empty(3,2)");
		assert.equal(t.at(1,3).data, "empty(1,2)-1,3");
		assert.equal(t.at(2,3).data, "empty(2,3)");
		assert.equal(t.at(3,3).data, "empty(3,3)");



		t.insert("test4",1,1, "down", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test4");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "empty(1,1)-2,1-3,1");
		assert.equal(t.at(1,2).data, "test3-1,2");
		assert.equal(t.at(2,2).data, "empty(2,2)");
		assert.equal(t.at(3,2).data, "empty(3,2)");
		assert.equal(t.at(1,3).data, "test2-1,2-1,3");
		assert.equal(t.at(2,3).data, "empty(2,3)");
		assert.equal(t.at(3,3).data, "empty(3,3)");
		assert.equal(t.at(1,4).data, "empty(1,2)-1,3-1,4");
		assert.equal(t.at(2,4).data, "empty(2,4)");
		assert.equal(t.at(3,4).data, "empty(3,4)");


		t.insert("test5",2,2, "down", true, function(data) {
			if (data.data) {
				data.data = data.data + "-" + data.x + "," + data.y;
			} else {
				data.data = data.x + "," + data.y;
			}
		});

		assert.equal(t.at(1,1).data, "test4");
		assert.equal(t.at(2,1).data, "test1-2,1");
		assert.equal(t.at(3,1).data, "empty(1,1)-2,1-3,1");
		assert.equal(t.at(1,2).data, "test3-1,2");
		assert.equal(t.at(2,2).data, "test5");
		assert.equal(t.at(3,2).data, "empty(3,2)");
		assert.equal(t.at(1,3).data, "test2-1,2-1,3");
		assert.equal(t.at(2,3).data, "empty(2,2)-2,3");
		assert.equal(t.at(3,3).data, "empty(3,3)");
		assert.equal(t.at(1,4).data, "empty(1,2)-1,3-1,4");
		assert.equal(t.at(2,4).data, "empty(2,3)-2,4");
		assert.equal(t.at(3,4).data, "empty(3,4)");
	});

QUnit.test("Bug 1", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:"", x:data.x, y:data.y};
		});

		t.resize(1,1);

		t.insert({name:"c1", x:1, y:1},1, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		assert.equal(t.row_count,2);
		assert.equal(t.col_count,1);

		t.insert({name:"c2", x:1, y:1},1, 1, "right", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c3", x:1, y:1},1, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});


		t.insert({name:"c4", x:2, y:2},2, 2, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		var data = t.at(2,1);
		var c1 = t.at(2,1).data;
		assert.propEqual({name:c1.name, x:c1.x, y:c1.y}, {name:"c1", x:2, y:1}, "c1 is good");


		assert.equal(t.at(1,2).data.name, "c2", "c2 is good");
		assert.equal(t.at(1,2).data.x, 1, "c2 is good");
		assert.equal(t.at(1,2).data.y, 2, "c2 is good");

		assert.equal(t.at(1,1).data.name, "c3", "c3 is good");
		assert.equal(t.at(1,1).data.x, 1, "c3 is good");
		assert.equal(t.at(1,1).data.y, 1, "c3 is good");

		assert.equal(t.at(2,2).data.name, "c4", "c4 is good");
		assert.equal(t.at(2,2).data.x, 2, "c4 is good");
		assert.equal(t.at(2,2).data.y, 2, "c4 is good");
	});

	QUnit.test("Bug 2", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:"", x:data.x, y:data.y};
		});

		t.resize(2,1);

		t.insert({name:"c1", x:1, y:1},1, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c2", x:1, y:1},1, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c3", x:1, y:1},1, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c4", x:2, y:1},2, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c5", x:2, y:1},2, 1, "down", true, function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		t.insert({name:"c6", x:2, y:1},2, 1, "down", function(data){
			data.data.x = data.x;
			data.data.y = data.y;
		});

		assert.equal(t.row_count, 7);
		assert.equal(t.col_count, 2);
	});

	QUnit.test("remove 1", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.propEqual(t.at(1,1).data, {name:"1,1",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});

		t.remove(1,1,"up")

		assert.propEqual(t.at(1,1).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});
	});

	QUnit.test("remove 2", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.propEqual(t.at(1,1).data, {name:"1,1",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});

		t.remove(1,1,"up", function(data){
			var d = data.data;
			d.x = data.x;
			d.y = data.y;
		});

		assert.propEqual(t.at(1,1).data, {name:"1,2",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});
	});


	QUnit.test("remove 3", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.propEqual(t.at(1,1).data, {name:"1,1",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});

		t.remove(1,1,"left");

		assert.propEqual(t.at(1,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});
	});

	QUnit.test("remove 4", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.propEqual(t.at(1,1).data, {name:"1,1",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});

		t.remove(1,1,"left", function(data){
			var d = data.data;
			d.x = data.x;
			d.y = data.y;
		});

		assert.propEqual(t.at(1,1).data, {name:"2,1",x:1,y:1});
		assert.propEqual(t.at(1,2).data, {name:"1,2",x:1,y:2});
		assert.propEqual(t.at(2,1).data, {name:"2,1",x:2,y:1});
		assert.propEqual(t.at(2,2).data, {name:"2,2",x:2,y:2});
	});

	QUnit.test("bug negative", function( assert ) {
		var t = new Table(function(data){
			data.data = {name:data.x+","+data.y, x:data.x, y:data.y};
		});

		t.resize(2,2);

		assert.throws( function () {
			t.at(0,1);
		},"0,1 is not in the range", "1 - throw error");

		assert.throws( function () {
			t.at(1,0);
		},"1,0 is not in the range", "1 - throw error");

		t.at(1,1);

	});


	return {}
})