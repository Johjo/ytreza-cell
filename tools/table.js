include('tools.table', function() {
	function Table(func_default_data) {
		this.data = [];
		this.row_count = 0;
		this.col_count = 0;
		this.func_default_data = func_default_data;
	}

	Table.prototype.at = function(x, y) {
		var pos_x = x == "last" ? Math.max(this.col_count, 1) : x
		var pos_y = y == "last" ? Math.max(this.row_count, 1) : y

		if (pos_x <= 0 || pos_y <= 0) {
			throw pos_x + "," + pos_y + " is not in the range";
		}

		var xx = pos_x - 1;
		var yy = pos_y - 1;

		if (pos_x <= this.col_count) {
			if (pos_y <= this.row_count) {
				if (!this.data[xx]) {this.data[xx] = [];}
				if (!this.data[xx][yy]) {
					this.data[xx][yy] = {data:null,x:pos_x,y:pos_y};
				}
				this.set_default_data(this.data[xx][yy])

				return this.data[xx][yy];
			}
		}

		return null;
	};

	Table.prototype.set_default_data = function(data) {
		if (!data.data) {
			if (this.func_default_data) {
				this.func_default_data(data);
			}
		}
	};


	Table.prototype.resize = function(x, y) {
		this.col_count = x;
		this.row_count = y;
	};

	/**
	 * insert data in the table and move cell
	 * @param  data to insert in the table
	 * @param  x : pos x  in the table
	 * @param  y : pos y in the table
	 * @param  direction : the direction where the cells move (can bu 'down' or 'right')
	 * @param  redim : true if the table's size can change, false else
	 * @param  func_update : function to update moved cell
	 * @return {[type]}
	 */
	Table.prototype.insert = function(data, x, y, direction, redim, func_update) {
		if (this.col_count < x || x < 1 || this.row_count < y || y <1){
			throw x+","+y + " is not in the range";
		}

		switch(direction) {
			case "right" :
				/* add a col if there's data in the last cell  */
				if (redim) {this.col_count++;}

				for (var i = this.col_count - 1; i >= x; i--) {
					this.at(i + 1, y).data = this.at(i, y).data;
					if (func_update) {
						var d = this.at(i + 1, y);
						func_update(d);
					}
				};
				break;
			case "down":
				/* add a row if there's data in the last cell */
				if (redim) {this.row_count++;}

				for (var i = this.row_count - 1; i >= y; i--) {
					this.at(x , i + 1).data = this.at(x, i).data;
					if (func_update) {
						var d = this.at(x, i + 1);
						func_update(d);
					}
				};
				break;
			default:
				throw "bad direction ! '" + direction + "'";
		}

		this.at(x,y).data = data;
	};


	Table.prototype.remove = function(x, y, direction, func_update) {
		switch(direction) {
			case "up":
				for (var yy = y; yy < this.row_count; yy++) {
					this.at(x, yy).data = this.at(x, yy + 1).data;
					if (func_update) {
						func_update(this.at(x, yy));
					}
				};

				this.at(x, "last").data = null;

				break;

			case "left" :
				for (var xx = x; xx < this.col_count; xx++) {
					this.at(xx, y).data = this.at(xx + 1, y).data;
					if (func_update) {
						func_update(this.at(xx, y));
					}
				};

				this.at("last", y).data = null;
				break;
			default:
				throw "bad direction ! '" + direction + "'";
		}
	};



	return Table;
})

