var factory;

function TestFactory() {
	this.last_cell = 0;
	this.cells = {};
}

TestFactory.prototype.create = function() {
	this.last_cell = this.last_cell + 1;
	data = {name:"c" + this.last_cell, content:""};

	var cell = new Cell(data, this);
	this.cells[cell.get_name()] = cell;
	return cell;
};

TestFactory.prototype.get = function(name) {
	return this.cells[name];
};

module('cell.html',{
    setup: function() {
    	factory = new TestFactory();
    }
});

QUnit.test("function create_cell", function( assert ) {
	var factory = new TestFactory();
	var c1 = factory.create();
	var c2 = factory.create();
	assert.equal(c1.get_name(), "c1", "c1.get_name() = c1");
	assert.equal(c2.get_name(), "c2", "c2.get_name() = c2");
});

module('Class Cell',{
    setup: function() {
        factory = new TestFactory();
    }
});

QUnit.test("new Cell()", function( assert ) {
	var c = new Cell({name:"c"});

	assert.equal(c.get_name(), "c", "c.get_name() = c");
	assert.equal(c.get_row_count(), 0, "c.get_row_count() = 0");
	assert.equal(c.get_col_count(), 0, "c.get_col_count() = 0");
});

QUnit.test("Cell and children", function( assert ) {
	var factory = new TestFactory();
	var c1 = factory.create();

	assert.throws(function() {c1.get_child(1,3)},CellException);
	var c2 = c1.create_child(1,3);
	assert.throws(function() {c1.create_child(1,3)},CellException);
	assert.ok(c2);

	assert.ok(c1.get_child(1,3), "1-3", 'c1.get_child(1,3).get_content() = "1-3"');
	assert.equal(c1.get_row_count(), 1, "c1.get_row_count() = 1");
	assert.equal(c1.get_col_count(), 3, "c1.get_col_count() = 3");

	c1.create_child(2,5);
	assert.ok(c1.get_child(2,5), "2-5", 'c1.get_child(2,5) = "2-5"');
	assert.equal(c1.get_row_count(), 2, "c1.get_row_count() = 2");
	assert.equal(c1.get_col_count(), 5, "c1.get_col_count() = 5");

	assert.ok(c1.get_child(1,4), "1-4", 'c1.get_child(1,4) = "1-4"');
	assert.equal(c1.get_row_count(), 2, "c1.get_row_count() = 2");
	assert.equal(c1.get_col_count(), 5, "c1.get_col_count() = 5");
});

QUnit.test("Taille des lignes et colonnes", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	assert.equal(c1.get_row_count(), 0, "aucune ligne");
	assert.equal(c1.get_col_count(), 0, "aucune colonne");

	var c2 = c1.create_child(1,1);
	c2.set_content("c2");
	assert.equal(c1.get_row_count(), 1, "1 seule ligne");
	assert.equal(c1.get_col_count(), 1, "1 seule colonne");

	var c3 = c1.create_child(1,2);
	c3.set_content("c3");
	assert.equal(c1.get_row_count(), 1, "1 seule ligne");
	assert.equal(c1.get_col_count(), 2, "2 colonnes");

	assert.ok(c1.has_child(1,1), "c1 a un enfant en (1,1)");
	assert.ok(c1.has_child(1,2), "c1 a un enfant en (1,2)");
	assert.ok(!c1.has_child(2,1), "c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2), "c1 n'a pas d'enfant en (2,2)");
});

module('Class Table',{
});


function table_default(x,y) {
	return "" + x + y;
}

QUnit.test("new Table", function( assert ) {
	var t1 = new Table();
	assert.ok(t1.rows, "t1.rows exist");
	assert.equal(t1.row_count,0,"t1.row_count = 0");
	assert.equal(t1.col_count,0,"t1.col_count = 0");
	assert.equal(t1.func_default_value,null,"t1.func_default_value doesn't exist");
});

QUnit.test("Table.set", function( assert ) {
	var t1 = new Table(table_default);

	t1.set("toto", 3, 5);
	assert.equal(t1.row_count,3,"t1.row_count = 3");
	assert.equal(t1.col_count,5,"t1.col_count = 5");
	assert.equal(t1.rows[2][4],"toto",'t1.rows[2][4]="toto"')

	t1.set("titi", 1, 2);
	assert.equal(t1.row_count,3,"t1.row_count = 3");
	assert.equal(t1.col_count,5,"t1.col_count = 5");
	assert.equal(t1.rows[0][1],"titi",'t1.rows[0][1]="titi"')

	t1.set("tutu", 5, 8);
	assert.equal(t1.row_count,5,"t1.row_count = 5");
	assert.equal(t1.col_count,8,"t1.col_count = 8");
	assert.equal(t1.rows[4][7],"tutu",'t1.rows[4][7]="tutu"')
});

QUnit.test("Table.get", function( assert ) {
	var t1 = new Table();
	var t2 = new Table(table_default);

	t1.set("toto", 3, 5);
	t2.set("toto", 3, 5);

	assert.equal(t1.row_count,3,"t1.row_count = 3");
	assert.equal(t1.col_count,5,"t1.col_count = 5");
	assert.equal(t1.rows[2][4],"toto",'t1.rows[2][4]="toto"')
	assert.equal(t2.rows[2][4],"toto",'t1.rows[2][4]="toto"')
	assert.equal(t1.get(3,5),"toto","t1.get(3,5)")
	assert.equal(t2.get(3,5),"toto","t2.get(3,5)")

	t1.set("titi", 1, 2);
	t2.set("titi", 1, 2);
	assert.equal(t1.row_count,3,"t1.row_count = 3");
	assert.equal(t1.col_count,5,"t1.col_count = 5");
	assert.equal(t1.rows[0][1],"titi",'t1.rows[0][1]="titi"')
	assert.equal(t1.get(1,2),"titi","t1.get(1,2)")
	assert.equal(t2.get(1,2),"titi","t2.get(1,2)")

	assert.equal(t1.get(3,1),null,"t1.get(3,1)");
	assert.equal(t2.get(3,1),"31",'t2.get(3,1) = "31"');
});

QUnit.test("Table.has", function( assert ) {
	var def = function() {
		return "default";
	}

	var t1 = new Table();
	var t2 = new Table(def);

	t1.set("toto", 3, 5);

	assert.ok(t1.has(3,5), "item at 3,5 is in the table");
	assert.ok(t1.has(2,3), "item at 2,3 is in the table");
	assert.ok(! t1.has(3,6), "item at 3,6 is not in the table");
	assert.ok(! t1.has(4,5), "item at 4,5 is not in the table");
});