module('htmltools',{
    setup: function() {
    }
});

QUnit.test("html_to_zen_coding", function( assert ) {
	var div = create_html("div","");
	assert.equal(html_to_zen_coding(div), "DIV");
	
	var div2 = create_html("div","class1");
	div2.id = "toto"
	div.appendChild(div2);
	assert.equal(html_to_zen_coding(div), "DIV>(DIV#toto.class1)");

	var div3 = create_html("div","class2");
	div.appendChild(div3);
	assert.equal(html_to_zen_coding(div), "DIV>(DIV#toto.class1&DIV.class2)");

	var div4 = create_html("div","");
	div2.appendChild(div4);
	assert.equal(html_to_zen_coding(div), "DIV>(DIV#toto.class1>(DIV)&DIV.class2)");

	var div5 = create_html("div","class3");
	div3.appendChild(div5);
	assert.equal(html_to_zen_coding(div), "DIV>(DIV#toto.class1>(DIV)&DIV.class2>(DIV.class3))");
});
