function MockModelManager() {
	this.last_cell = 0;
	ModelsManager.call(this);

	this.cells = {};
	var c1 = this.create_cell();
	this.cells[c1.get_name()] = c1;

	var c2 = this.create_cell();;
	this.cells[c2.get_name()] = c2;

	var c3 = c2.create_child(1,1);
	this.cells[c3.get_name()] = c3;
}

MockModelManager.prototype = new ModelsManager;

MockModelManager.prototype.create_data = function(name) {
	var data = {name: name, content: name};
	return new CellModel(data, this)
};

MockModelManager.prototype.create_cell = function() {
	this.last_cell = this.last_cell + 1;

	data = {name:"c" + this.last_cell};
	var cell = new CellModel(data, this);
	return cell;
};

MockModelManager.prototype.get_cell = function(name) {
	return this.cells[name];
};

module('Controller',{
    setup: function() {
    }
});