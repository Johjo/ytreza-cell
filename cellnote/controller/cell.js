include('cellnote.controller.cell', function() {
	function Controller() {
		this.view = [];
	}

	Controller.prototype.set_model_manager = function(model_manager) {
		this.model_manager = model_manager;
	};

	Controller.prototype.add_view = function(view) {
		this.view.push(view);
		view.show();
	};

	Controller.prototype.create_cell = function() {
		return this.model_manager.create_cell();
	};

	Controller.prototype.get_cell_model = function(name) {
		var model = this.model_manager.get_cell(name);

		return model;
	};

	Controller.prototype.update_content_cell = function(cm, new_content) {
		if (cm.get_content() != new_content) {
			cm.set_content(new_content);
			this.model_manager.save_cell(cm);
		}
	};

	Controller.prototype.update_archive = function(cell_model, archived) {
		if (cell_model.is_archived() != archived) {
			cell_model.set_archived(archived);
			this.model_manager.save_cell(cell_model);
		}
	};

	Controller.prototype.update_layout = function(cell_model, layout) {
		if (cell_model.get_layout() != layout) {
			cell_model.set_layout(layout);
			this.model_manager.save_cell(cell_model);
		}
	};

	Controller.prototype.add_column = function(cell_model) {
		this.create_child(cell_model, cell_model.get_col_count() + 1, 1, "right");
	};

	Controller.prototype.add_cell = function(cell_model, x) {
		var new_cell = this.model_manager.create_cell();
		cell_model.add_child_in_column(new_cell, x);
		this.model_manager.save_cell(cell_model);
		this.model_manager.save_cell(new_cell);
	};

	Controller.prototype.create_child = function(cell_model, x, y, direction) {
		var new_cell = this.model_manager.create_cell(cell_model,x,y);
		cell_model.insert_child(new_cell, x, y, direction);
		this.model_manager.save_cell(cell_model);
		this.model_manager.save_cell(new_cell);

		return new_cell;
	};

	Controller.prototype.move_cell = function(cm_moved, cm_new_father, cm_replaced, cm_old_father) {
		var pos = cm_new_father.get_position(cm_replaced);

		cm_old_father.remove_child(cm_moved, "up");
		cm_new_father.insert_child(cm_moved, pos.x, pos.y, "down");

		this.model_manager.save_cell(cm_old_father);
		this.model_manager.save_cell(cm_new_father);
	};

	return Controller;
})
