var deps = ['cellnote.models.models','cellnote.controller.cell', 'tools.tools']

include("tests_cell", deps, function(Model, Controller, Tools) {

	module('Controller - Cell',{
		setup: function() {
		}
	});

	QUnit.test("create_child", function( assert ) {
		var controller = new Controller();
		var storage = new Tools.MockStorage();
		controller.set_model_manager(new Model.ModelsManagerLocale(storage));

		var cm1 = controller.create_cell();

		controller.create_child(cm1, 1, 1, "right");

		var c2 = cm1.get_child(1,1);
		assert.equal(c2.get_name(), "c2", "c2 -> name is c2");
		assert.equal(c2.x, 1, "c2 -> x = 1");
		assert.equal(c2.y, 1, "c2 -> y = 1");
		assert.ok(c2.cm_parent, "c2 -> parent is not null");
		assert.equal(c2.cm_parent, cm1, "c2 -> parent is cm1");
	});


	QUnit.test("add_column", function( assert ) {
		var controller = new Controller();
		var storage = new Tools.MockStorage();
		controller.set_model_manager(new Model.ModelsManagerLocale(storage));

		var cm1 = controller.create_cell();

		controller.add_column(cm1);
		var c2 = cm1.get_child(1,1);
		assert.equal(c2.get_name(), "c2", "c2 -> name is c2");
		assert.equal(c2.x, 1, "c2 -> x = 1");
		assert.equal(c2.y, 1, "c2 -> y = 1");
		assert.equal(c2.cm_parent, cm1, "c2 -> parent is cm1");

		controller.add_column(cm1);
		var c3 = cm1.get_child(2,1);
		assert.equal(c3.name, "c3", "c3 -> name is c3");
		assert.equal(c3.x, 2, "c3 -> x = 2");
		assert.equal(c3.y, 1, "c3 -> y = 1");
		assert.equal(c3.cm_parent, cm1, "c3 -> parent is cm1");
	});

	QUnit.test("update_content_cell", function( assert ) {
		var storage = Tools.MockStorage();
		storage.clear();

		var controller = new Controller();
		controller.set_model_manager(new Model.ModelsManagerLocale(storage));

		var data = {name:"c0", children:[["c4"],["c2","c3"]]};
		storage["c0"] = JSON.stringify(data);

		var data = JSON.parse(storage["c0"]);
		assert.deepEqual(data,{name:"c0", children:[["c4"],["c2","c3"]],name: "c0"});

		var c0 = controller.get_cell_model("c0");
		c0.save();
		var c1 = c0.get_child(2,1);


		assert.equal(c1.get_name(), "", "c1 -> no name");
		assert.equal(c1.get_content(), "", "c1 -> no content");

		controller.update_content_cell(c1, "test content");
		assert.equal(c1.get_name(), "c1", "c1 -> name is ok");
		assert.equal(c1.get_content(), "test content", "c1 -> content update is ok");

		assert.deepEqual(c0.get_data(),{archived:false,name:"c0", children:[["c4","c1"],["c2","c3"]],col_count: 2,content: "",layout: 3,name: "c0",row_count:2});
		var data = JSON.parse(storage["c0"]);
		assert.deepEqual(data,{archived:false,name:"c0", children:[["c4","c1"],["c2","c3"]],col_count: 2,content: "",layout: 3,name: "c0",row_count:2});
	});

	return {}
})