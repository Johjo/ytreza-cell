var deps = ['cellnote.models.models', 'cellnote.view.cell','cellnote.controller.cell', 'tools.tools','cellnote.view.main_view', 'cellnote.view.cursor']

include("tests_cursors", deps, function(Model, CellView, Controller, Tools, MainView, Cursor) {
	module('View - Cell Cursor',{
		setup: function() {
		}
	});

	QUnit.test("test 1", function( assert ) {
		var controller = new Controller();
		var manager = new Model.ModelsManagerLocale(new Tools.MockStorage());
		controller.set_model_manager(manager);
		var main_view = new MainView(controller);

		var c1 = manager.create_cell();
		var c2 = controller.create_child(c1,1,1,"right"); /*c2*/
		var c3 = controller.create_child(c1,2,1,"right"); /*c3*/
		var c4 = controller.create_child(c1,3,1,"right"); /*c4*/
		var c5 = controller.create_child(c1,1,2,"right"); /*c5*/
		var c6 = controller.create_child(c1,2,2,"right"); /*c6*/
		var c7 = controller.create_child(c1,3,2,"right"); /*c7*/

		var cv1 = main_view.create_cell(c1);
		var cursor = new Cursor(main_view);

		cursor.set_position(cv1);
		assert.equal(cursor.get_cell().model.get_name(),c1.get_name(), "c1 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c2.get_name(), "go right : c2 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c3.get_name(), "go right : c3 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c4.get_name(), "go right : c4 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c4.get_name(), "go right : c4 is the current cell (no move)");

		cursor.go_down();
		assert.equal(cursor.get_cell().model.get_name(),c7.get_name(), "go down : c7 is the current cell");

		cursor.go_down();
		assert.equal(cursor.get_cell().model.get_name(),c7.get_name(), "go down : c7 is the current cell (no move)");

		cursor.go_down();
		assert.equal(cursor.get_cell().model.get_name(),c7.get_name(), "go down : c7 is the current cell (no move)");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c6.get_name(), "go left : c6 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c5.get_name(), "go left : c5 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c1.get_name(), "go left : c1 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c1.get_name(), "go left : c1 is the current cell (no move)");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c2.get_name(), "go right : D : c2 is the current cell");

		cursor.go_down();
		assert.equal(cursor.get_cell().model.get_name(),c5.get_name(), "go down : c5 is the current cell");

		cursor.go_up();
		assert.equal(cursor.get_cell().model.get_name(),c2.get_name(), "go up : c2 is the current cell");

		cursor.go_up();
		assert.equal(cursor.get_cell().model.get_name(),c1.get_name(), "go up : c1 is the current cell");

		var c8 = controller.create_child(c3,1,1,"right"); /*c8*/
		cursor.set_position(cv1);

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c2.get_name(), "go right : c2 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c3.get_name(), "go right : c3 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c8.get_name(), "go right : c8 is the current cell");

		cursor.go_right();
		assert.equal(cursor.get_cell().model.get_name(),c4.get_name(), "go right : c4 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c8.get_name(), "go left : c8 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c3.get_name(), "go left : c3 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c2.get_name(), "go left : c2 is the current cell");

		cursor.go_left();
		assert.equal(cursor.get_cell().model.get_name(),c1.get_name(), "go left : c1 is the current cell");

	});

	QUnit.test("test 1", function( assert ) {
		var controller = new Controller();
		var manager = new Model.ModelsManagerLocale(new Tools.MockStorage());
		controller.set_model_manager(manager);
		var main_view = new MainView(controller);

		var c1 = manager.create_cell();
		c1.table.resize(2,2);

		var cv1 = main_view.create_cell(c1);
		var cursor = new Cursor(main_view);

		function convert(cm) {
			return {name:cm.get_name(), x: cm.x, y:cm.y};
		}

		cursor.set_position(cv1);
		assert.propEqual(convert(cursor.get_cell().model), {name:'c1',x:1,y:1}, "1st step, ok");

		cursor.go_right();
		assert.propEqual(convert(cursor.get_cell().model), {name:'',x:1,y:1}, "2nd step, ok");

		cursor.go_right();
		assert.propEqual(convert(cursor.get_cell().model), {name:'',x:2,y:1}, "3rd step, ok");

		cursor.go_down();
		assert.propEqual(convert(cursor.get_cell().model), {name:'',x:2,y:2}, "4th step, ok");

		cursor.go_left();
		assert.propEqual(convert(cursor.get_cell().model), {name:'',x:1,y:2}, "5th step, ok");

		console.log("here");
		cursor.go_left();
		assert.propEqual(convert(cursor.get_cell().model), {name:'c1',x:1,y:1}, "6th step, ok");
	});



	return {}
})