include('cellnote.view.cell', ['cellnote.view.mode'], function(Mode) {
	function CellViewBis(controller, model, parent, view) {
		riot.observable(this);

		this.controller = controller;
		this.model = model;
		this.parent = parent;

		this.level = (this.parent) ? this.parent.get_level()  + 1 : 1;
		this.view = view;

		this.set_classname("cell");
		this.name = this.model.get_name();

		this.children = [];

		this.set_mode(new Mode.ModeDisplay());
		this.edited = false;
		this.selected = false;

		this.item = null;
	}

	CellViewBis.prototype.set_item = function(item) {
		this.item = item;
	};

	CellViewBis.prototype.get_parent = function() {
		return this.parent;
	};

	CellViewBis.prototype.get_level = function() {
		return this.level;
	};

	CellViewBis.prototype.get_view = function() {
		return this.view;
	};

	CellViewBis.prototype.get_model_for_update = function() {
		return this.model;
	};

	CellViewBis.prototype.get_layout = function() {
		var layout = "";
		switch(this.model.get_layout()) {
			case LAYOUT_LIST_HORIZONTAL : layout = "list_horizontal"; break;
			case LAYOUT_LIST_VERTICAL : layout = "list_vertical"; break;
		}

		return layout;
	}

	CellViewBis.prototype.get_odd_or_even = function() {
		return (this.get_level() % 2 == 0) ? "even" : "odd";
	};

	CellViewBis.prototype.get_path = function() {
		var parent_path = this.parent ? this.parent.get_path() : "";
		if (parent_path !== "") {
			return this.parent.get_path() + "." + this.name;
		} else {
			return this.name;
		}
	};

	CellViewBis.prototype.set_mode = function(mode) {
		this.mode = mode;
		this.set_classname(this.mode.get_class_name());

		this.trigger("update", {field : 'possibility'});
	};

	CellViewBis.prototype.get_children = function() {
		var view = this;
		var children = this.model.get_unarchived_children();

		var result = [];

		for (var i = 0; i < children.length; i++) {
			var child = children[i];

			child.on('update', function(detail) {
				switch(detail.field) {
					case "archived" : this.trigger("update", {field : 'children'}); break;
					default : break;
				}
			}.bind(this));
			result.push({type:"intercell", path: view.get_path() + "." + child.get_name()});
			result.push({type:"cell", path: view.get_path() + "." + child.get_name()});
		};

		return result;
	};

	CellViewBis.prototype.refresh = function(parent) {
		if (parent && this.parent) {
			this.parent.refresh();
		}

		if (this.item) {
			this.item.update();
		}
	};

	CellViewBis.prototype.has_children = function() {
		return this.model.has_children();
	};

	CellViewBis.prototype.has_brother = function(x,y) {
		if (this.parent) {
			return this.parent.has_child_at(x, y);
		} else {
			return false;
		}
	};

	CellViewBis.prototype.get_brother = function(x,y) {
		if (this.parent) {
			return this.parent.get_child(x, y);
		} else {
			return null;
		}
	};


	CellViewBis.prototype.has_child_at = function(x,y) {
		try {
			return this.model.get_child(x,y) != null;
		} catch(e) {
			return false;
		}
	};

	CellViewBis.prototype.can_add_child = function() {
		if (this.level >= 6) {return false;};

		return true;
	};

	CellViewBis.prototype.can_add_brother = function() {
		if (this.level <= 1) {return false;};

		return true;
	};

	CellViewBis.prototype.can_archive = function() {
		if (this.level <= 1)  {return false;};
		if (this.get_model_for_update().is_archived()) {return false;};
		return false;
		return true;
	};

	CellViewBis.prototype.can_drag_over = function(path_src) {


		if (this.get_path().indexOf(path_src) >= 0) { return false };	/* the child can't become the father */

		return true;
	};


	CellViewBis.prototype.is_row = function() {
		if (this.parent) {
			return !this.parent.is_row();
		} else {
			return true;
		}
	};

	CellViewBis.prototype.can_build_children = function() {
		return this.level < 6;
	};

	CellViewBis.prototype.can_edit = function() {
		return this.mode.can_edit();
	};

	CellViewBis.prototype.can_create_child = function() {
		if (this.level >= 6) {return false;};

		return true;
	};

	CellViewBis.prototype.can_mouse_over = function() {
		if (! this.mode.can_mouse_over()) {return false};
		if (this.view.dragging) {return false};

		return true;
	};

	CellViewBis.prototype.get_child = function(x,y) {
		var cm = this.model.get_child(x,y);
		return this.view.get_cell_from_model(cm);
	};

	CellViewBis.prototype.get_position = function() {
		var model = this.model;
		return {x:model.x, y:model.y};
	};

	CellViewBis.prototype.event = function(func) {
		var cv = this;
		return function(event) {
			if (!event.done) {
				event.done = true;
				var res = func.call(cv, event);
				if (typeof res !== "undefined") {
					return res;
				}
			} else {
				return true;
			}
		}.bind(this);
	};

	CellViewBis.prototype.get_actions = function() {
		var actions = [];
		if (this.children.length==0) {actions.push({name:"create_first_child", event:this.event(this.on_create_first_child)})};
		if (this.children.length>0) {actions.push({name:"add_column", event:this.event(this.on_add_column)})};
		if (this.level>1) {actions.push({name:"add_column_to_parent", event:this.event(this.on_add_column_to_parent)})};
		if (this.level>1) {actions.push({name:"add_cell_to_parent", event:this.event(this.on_add_cell_to_parent)})};
		if (this.can_edit()) {actions.push({name:"edit", event:this.event(this.on_edit)})};

		return actions;
	};

	CellViewBis.prototype.on_create_first_child = function(event) {
		var cell_model = this.get_model_for_update();
		this.controller.create_child(cell_model,1,1,"down");
		this.item.update();
	};

	CellViewBis.prototype.on_add_column = function(event) {
		cell_model = this.get_model_for_update();
		this.controller.add_column(cell_model);
	};

	CellViewBis.prototype.on_add_column_to_parent = function(event) {
		if (this.level > 1 && this.parent) {
			this.parent.on_add_column(event);
		}
	};

	CellViewBis.prototype.on_add_cell_to_parent = function(event) {
		if (this.level > 1 && this.parent) {
			var x = this.item.x;

			var cell_model = this.parent.get_model_for_update();
			this.controller.add_cell(cell_model,x);
			this.parent.item.update();
		}
	};

	CellViewBis.prototype.set_classname = function(classname) {
		this.classname = classname;
		this.trigger("update", {field : 'classname', value: classname});
	};

	/***** EVENT ******/
	CellViewBis.prototype.on_add_column = function() {
		var model = this.model;
		var controller = this.controller;
		controller.add_column(model);
		this.refresh();
	};

	CellViewBis.prototype.on_add_child = function(event) {
		var cell_model = this.get_model_for_update();

		var x = event.item.opts.x;
		var y = event.item.opts.y;
		var type = event.item.opts.type;

		switch(type){
			case "add_bottom":
				this.controller.create_child(cell_model,x,y,"down");
				break;
			case "add_right":
				this.controller.create_child(cell_model,x,y,"right");
				break;
		}
	};

	CellViewBis.prototype.on_add_brother_right = function(event) {
		if (this.parent) {
			var cell_model = this.parent.get_model();
			var x = event.item.opts.x;
			var y = event.item.opts.y;

			this.controller.create_child(cell_model,x+1,y,"right");
		}
	};

	CellViewBis.prototype.on_add_brother_bottom = function(event) {
		if (this.parent) {
			var cell_model = this.parent.get_model();
			var x = event.item.opts.x;
			var y = event.item.opts.y;

			this.controller.create_child(cell_model,x,y+1,"down");
		}
	};

	CellViewBis.prototype.on_mouse_over = function() {
/*		if (this.can_mouse_over()) {
			this.set_mode(new Mode.ModeMouseOver(this));
			this.view.on_mouse_over(this);
		}*/
	};

	CellViewBis.prototype.on_mouse_out = function() {
		if (this.mode.can_mouse_out()) {
			this.set_mode(new Mode.ModeDisplay(this));
		}
	};



	CellViewBis.prototype.on_edit = function() {
		if (this.mode.can_edit()) {
			this.edited = true;
			this.set_mode(new Mode.ModeEdit(this));
			this.view.on_edit(this);
			this.trigger("action");
		}
	};

	CellViewBis.prototype.on_save_content = function() {
		this.controller.update_content_cell(this.get_model_for_update(), this.item.field_content.value);

		this.edited = false;
		this.trigger("action");
	};

	CellViewBis.prototype.on_context_menu = function(event) {
		/* select the cell */
		this.trigger("show_menu",this,event);


		return false;
	};

	CellViewBis.prototype.unselect = function() {
		this.selected = false;
		this.trigger("on_unselect", {cell:this});
	}


	CellViewBis.prototype.select = function() {
		this.selected = true;
		this.trigger("on_select", {cell:this,add:false});
	}

	CellViewBis.prototype.add_to_selection = function() {
		this.selected = true;
		this.trigger("on_select", {cell:this,add:true});
	}

	CellViewBis.prototype.edit = function() {
			this.edited = true;
			this.set_mode(new Mode.ModeEdit(this));
			this.view.on_edit(this);
			this.trigger("action");
	};

	CellViewBis.prototype.on_select = function(event) {
		this.trigger("select", this);
	};

	CellViewBis.prototype.on_cancel_edit = function() {
		if (this.mode.can_cancel_edit()) {
			this.set_mode(new Mode.ModeDisplay(this));
		}
	};

	CellViewBis.prototype.on_focus = function() {
		if (this.mode.can_focus()) {
			this.view.on_focus(this);
		}
	};

	CellViewBis.prototype.on_archive = function() {
		if (this.can_archive()) {
			this.controller.update_archive(get_model_for_update(), true);
		}
	};

	CellViewBis.prototype.on_change_layout = function() {
		switch(this.model.get_layout()) {
			case LAYOUT_LIST_HORIZONTAL : this.controller.update_layout(this.model,LAYOUT_LIST_VERTICAL); break;
			case LAYOUT_LIST_VERTICAL : this.controller.update_layout(this.model,LAYOUT_LIST_HORIZONTAL); break;
		}
	};

	CellViewBis.prototype.on_drag_start = function(event) {
		this.view.on_drag_start();
		event.dataTransfer.setData("text", this.model.get_content());
		event.dataTransfer.setData("path", this.get_path());

		return true;
	};

	CellViewBis.prototype.on_drag_stop = function(event) {
		this.view.on_drag_stop();
		return true;
	};

	CellViewBis.prototype.on_drag_over = function(event) {
		var path_src = event.dataTransfer.getData("path");

		if (this.can_drag_over(path_src)) {
			event.dataTransfer.dropEffect = 'move';
			event.preventDefault();
		}
		return true;
	};

	CellViewBis.prototype.on_drop = function(event) {
		event.preventDefault();
		var path_src = event.dataTransfer.getData("path");
		var cv_moved = this.view.get_cell_from_model(path_src);

		if (cv_moved) {
			var cm_moved = cv_moved.model;
			var cm_replaced = this.model;

			var cm_new_father = this.parent.model;
			var cm_old_father = cv_moved.parent.model;

			this.controller.move_cell(cm_moved, cm_new_father, cm_replaced, cm_old_father);
		}

		return true;
	};


	CellViewBis.prototype.get_can = function() {
		var can = {
			edit : this.can_edit(),
			create_child : this.can_create_child(),
			archive : this.can_archive(),
			show_children : this.can_build_children()
		}

		return can;
	};

	CellViewBis.prototype.get_event = function() {
		var event = {
			on_mouse_over : this.event(this.on_mouse_over),
			on_mouse_out : this.event(this.on_mouse_out),
			on_edit : this.event(this.on_edit),
			on_focus : this.event(this.on_focus),
			on_add_column : this.event(this.on_add_column),
			on_add_child : this.event(this.on_add_child),
			on_archive : this.event(this.on_archive),
			on_save_content : this.event(this.on_save_content),
			on_change_layout : this.event(this.on_change_layout),
			on_drag_start : this.event(this.on_drag_start),
			on_drag_over : this.event(this.on_drag_over),
			on_drop : this.event(this.on_drop),
			on_drag_stop : this.event(this.on_drag_stop),
			on_add_brother_right : this.event(this.on_add_brother_right),
			on_add_brother_bottom : this.event(this.on_add_brother_bottom),
			on_select: this.event(this.on_select),
			on_context_menu: this.event(this.on_context_menu)
		}

		return event;
	};

	return CellViewBis;
})
