include('cellnote.view.tools', function() {
	var regexp_path = new RegExp("^(.*)[.]([^.]*)$");
	function analyze_path(path) {
		var parent = "";
		var name = "";

		if (path!==undefined && path!=="" && path!==".") {
			var matches = regexp_path.exec(path);

			if (matches && matches.length == 3) {
				parent = matches[1];
				name = matches[2];
			} else {
				parent="";
				name = path;
			}
		}

		return {
			name:name, 
			parent:parent
		}
	}

	return {
		analyze_path : analyze_path
	}
})
