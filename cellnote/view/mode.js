include('cellnote.view.mode', function() {
	function Mode(cellview, class_name) {
		this.cellview = cellview;
		if (cellview) {this.div = cellview.div;}
		this.displayed = false;
		this.class_name = class_name;
	}

	Mode.prototype.get_class_name = function() {
		return this.class_name;
	};

	Mode.prototype.can_display = function() {return false;};
	Mode.prototype.can_edit = function() {return false;};
	Mode.prototype.can_mouse_over = function() {return false;};
	Mode.prototype.can_mouse_out = function() {return false;};
	Mode.prototype.can_select = function() {return false;};
	Mode.prototype.can_unselect = function() {return false;};
	Mode.prototype.can_use_edit = function() {return false;};
	Mode.prototype.can_use_see = function() {return false;};
	Mode.prototype.can_edit_child = function() {return false;};
	Mode.prototype.can_cancel_edit = function() {return false;};
	Mode.prototype.can_focus = function() {return false;};
	Mode.prototype.can_add_child = function() {return false;};
	Mode.prototype.can_save_content = function() {return false;};

/****************************************
ModeDisplay
****************************************/

	function ModeDisplay(cellview) {
		Mode.call(this, cellview,"cell");
	}

	ModeDisplay.prototype = new Mode;

	ModeDisplay.prototype.can_edit = function() {return true;};
	ModeDisplay.prototype.can_select = function() {return true;};
	ModeDisplay.prototype.can_mouse_over = function() {return true;};
	ModeDisplay.prototype.can_edit_child = function() {return true;};
	ModeDisplay.prototype.can_focus = function() {return true;};

/****************************************
ModeEdit
****************************************/

	function ModeEdit(cellview,div) {
		Mode.call(this, cellview,"edited_cell");
	}

	ModeEdit.prototype = new Mode;
	ModeEdit.prototype.can_cancel_edit = function() {return true;};
	ModeEdit.prototype.can_save_content = function() {return true;};

/****************************************
ModeDraggable
****************************************/

	function ModeDraggable(cellview,div) {
		Mode.call(this, cellview,"dragged_cell");
	}

	ModeDraggable.prototype = new Mode;

/****************************************
ModeMouseOver
****************************************/
	function ModeMouseOver(cellview) {
		Mode.call(this, cellview,"over_cell");
	}

	ModeMouseOver.prototype = new Mode;

	ModeMouseOver.prototype.can_display = function() {return true;};
	ModeMouseOver.prototype.can_edit = function() {return true;};
	ModeMouseOver.prototype.can_select = function() {return true;};
	ModeMouseOver.prototype.can_mouse_out = function() {return true;};
	ModeMouseOver.prototype.can_use_edit = function() {return true;};
	ModeMouseOver.prototype.can_use_see = function() {return true;};
	ModeMouseOver.prototype.can_edit_child = function() {return true;};
	ModeMouseOver.prototype.can_focus = function() {return true;};
	ModeMouseOver.prototype.can_add_child = function() {return true;};

	return {
		ModeDisplay : ModeDisplay,
		ModeEdit : ModeEdit,
		ModeMouseOver : ModeMouseOver
	}
})
