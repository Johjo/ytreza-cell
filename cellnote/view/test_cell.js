function MockModelManager() {
	this.last_cell = 0;
	ModelsManager.call(this);

	this.cells = {};
	var c1 = this.create_cell();
	this.cells[c1.get_name()] = c1;

	var c2 = this.create_cell();;
	this.cells[c2.get_name()] = c2;

	var c3 = c2.create_child(1,1);
	this.cells[c3.get_name()] = c3;
}

MockModelManager.prototype = new ModelsManager;

MockModelManager.prototype.create_data = function(name) {
	var data = {name: name, content: name};
	return new CellModel(data, this)
};

MockModelManager.prototype.create_cell = function() {
	this.last_cell = this.last_cell + 1;

	data = {name:"c" + this.last_cell};
	var cell = new CellModel(data, this);
	return cell;
};

MockModelManager.prototype.get_cell = function(name) {
	return this.cells[name];
};

var main_div;
var controller;
var view;

module('View - Cell',{
    setup: function() {
		controller = new Controller();
		controller.set_model_manager(new MockModelManager());
    	
    	main_div = create_html("DIV","main");
    }
});

QUnit.test("CellView first cell", function( assert ) {
	view = new MainView(controller, "c1", main_div);
	view.init();
	var main_cell = view.main_cell;

	assert.equal(main_cell.get_html_zen_coding(), 
		"DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))");
	assert.equal(html_to_zen_coding(main_div), 
		"DIV.main>(DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON)))))");
});

QUnit.test("CellView cell with one child", function( assert ) {
	view = new MainView(controller, "c2", main_div);
	view.init();
	var main_cell = view.main_cell;

	assert.equal(main_cell.get_html_zen_coding(), 
		"DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.row>(DIV>(DIV.row>(DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))))&DIV.new_child>(DIV>(BUTTON)))&DIV.row>(DIV.new_child>(DIV>(BUTTON))&DIV.new_child>(DIV>(BUTTON)))))");
	assert.equal(html_to_zen_coding(main_div), 
		"DIV.main>(DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.row>(DIV>(DIV.row>(DIV.cell>(P.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))))&DIV.new_child>(DIV>(BUTTON)))&DIV.row>(DIV.new_child>(DIV>(BUTTON))&DIV.new_child>(DIV>(BUTTON))))))");
});

