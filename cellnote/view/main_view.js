var deps = ['cellnote.view.cell','cellnote.view.view','cellnote.view.tools',
			'cellnote.view.table_view', 'cellnote.view.cell_event', 'cellnote.view.keys', 'cellnote.view.cursor']



include('cellnote.view.main_view', deps, function(CellViewBis, View, Tools, TableView, Event, Keys, Cursor) {
	function MainView(controller, cell_name) {
		View.call(this,controller);
		riot.observable(this);

		this.cell_name = cell_name;

		this.cm_root = controller.get_cell_model(cell_name);

		this.main_cell_bis = new CellViewBis(controller,this.cm_root,this);
		this.cells = {};

		this.cells_selected = [];
		this.cursor = new Cursor();

		this.register_keys();
	}

	MainView.prototype = new View;

	MainView.prototype.refresh = function() {
	};

	MainView.prototype.get_cell_key = function(cm) {
		var key = "";
		if (cm.cm_parent) {key = cm.cm_parent.get_name()}
		key = key + "." + cm.get_name();
		key = key + "(" + cm.x + "," + cm.y +")";

		return key;
	};

	MainView.prototype.get_cell_from_model = function(cm) {
		if (!cm) {
			return null;
		} else {
			var key = this.get_cell_key(cm);
			if (key == "") {
				return this.create_cell(cm);
			} else {
				if (!this.cells[key]) {
					this.cells[key] = this.create_cell(cm);
				}
				return this.cells[key];
			}
		}
	};

	MainView.prototype.register_keys = function() {
		this.keys = new Keys(window, this);
	};


	MainView.prototype.create_cell = function(cm) {
		var cell = new CellViewBis(this.controller, cm, this.get_cell_from_model(cm.get_parent()),this);

		cell.on("on_select", function(detail){
			var is_selected = false;
			for (var i = 0; i < this.cells_selected.length; i++) {
				if (this.cells_selected[i] !== detail.cell) {
					/* unselect cell */
					if (!detail.add) {this.cells_selected[i].unselect();};
				} else {
					is_selected = true;
				}
			};

			if (!detail.add) {
				this.cells_selected = [];
				is_selected = false;
			}

			if (!is_selected) {this.cells_selected.push(detail.cell);};
			this.cursor.set_position(detail.cell);

		}.bind(this))

		cell.on("show_menu", function(cell,event) {
			this.trigger("show_menu", cell, event);
		}.bind(this))

		return cell;
	};

	MainView.prototype.get_path = function() {
		return "";
	};

	MainView.prototype.show = function() {
		var main_view = this;
		var controller = this.controller;

		riot.tag('tmpl-view', tmpl_view.innerHTML, function(opts) {
			this.view = main_view.get_cell_from_model(opts.model);
			this.model = opts.model;
			this.path = this.view.get_path();
			this.name = this.view.model.get_name();
			this.x = 1;
			this.y = 1;
			this.type = "cell"

			main_view.on('focus', function(path) {
				this.update();
			}.bind(this));
		});

		riot.tag('tmpl-children', tmpl_children.innerHTML, function(opts) {
			if (opts.view) {
				opts.event = opts.view.get_event();
			}
		});

		riot.tag('tmpl-action', tmpl_action.innerHTML, function(opts) {
			this.actions = [];

			main_view.on("show_menu", function(cell, event) {
				if (cell) {
					this.actions = cell.get_actions();
					this.cell = cell;
					opts.x = event.clientX + window.pageXOffset;
  					opts.y = event.clientY + window.pageYOffset;
					this.update();
				}
			}.bind(this))

		});

		/* tag tmpl-cell */
		riot.tag('tmpl-cell', tmpl_cell.innerHTML, function(opts) {
			if (opts.type!=="cell") {return};

			if (opts.model) {
				this.view = main_view.get_cell_from_model(opts.model);
			}

			this.model = opts.model;
			opts.event = this.view.get_event();

			this.view.set_item(this);

			this.name = this.view.name;
			this.level = this.view.level;

			/* show the children table */
			var table = new TableView(this.model, opts.path);
			this.children = table.children;

			table.on("update_child", function() {
				this.children = table.children;
				this.update();
			}.bind(this))


			this.content = this.model ? this.model.get_content() : "";
			this.archived = this.model ? this.model.is_archived() : "";

			this.classname = this.view.classname;
			this.type = this.view.get_odd_or_even();

			this.can_edit = this.view.can_edit();
			this.can_create_child = this.view.can_create_child();
			this.can_archive = this.view.can_archive();
			this.can_show_children = this.view.can_build_children();
			this.can_add_child = this.view.can_add_child();

			this.edited = this.view.edited;

			this.on_click_cell = Event.on_click_cell(this.view);
			this.on_click_edit = Event.on_click_edit(this.view);

			this.view.on("action", function() {
				this.edited = this.view.edited;
				this.update();
			}.bind(this));

			this.view.on("on_select", function() {
				this.selected = true;
				this.update();
			}.bind(this));

			this.view.on("on_unselect", function() {
				this.selected = false;
				this.update();
			}.bind(this));


			/*model update*/
			if (this.model) {
				this.model.on('update', function(detail) {
					switch(detail.field) {
						case "content"	: this.content = this.model.get_content(); break;
						case "archived" : this.archived = this.model.is_archived(); break;
						case "name"		: this.name = this.model.get_name(); break;
						default : console.log("unknown field", detail); break;
					}
					this.update();
				}.bind(this));
			}

			/*view update*/
			this.view.on('update', function(detail) {
				switch(detail.field) {
					case "classname" : this.classname = detail.value; break;
					case "type" : this.type = this.view.get_odd_or_even(); break;
					case "possibility" :
					this.can_edit = this.view.can_edit();
					this.can_create_child = this.view.can_create_child();
					this.can_archive = this.view.can_archive();
					this.can_show_children = this.view.can_build_children();
					this.can_add_child = this.view.can_add_child();
					break;
					default : console.log("unknown field", detail); break;
				}
				this.update();
			}.bind(this));

			// } catch(e){
			// 	console.log(opts);
			// 	console.log(e);
			// }
		})

	riot.mount('tmpl-view', {
		children : {
			row:{
				type:"cell",
				x:1,
				y:1,
				path:'',
				name: main_view.cell_name,
				model: main_view.cm_root
			}
		},
		model: main_view.cm_root,
		path : main_view.cell_name
})

riot.mount('tmpl-action', {
})
};

MainView.prototype.get_view = function() {
	return this;
};

MainView.prototype.get_level = function() {
	return 0;
};

MainView.prototype.is_row = function() {
	return false;
};


MainView.prototype.on_mouse_over = function(cell_view) {
	for (i in this.mouse_over) {
		var same_object = (cell_view===this.mouse_over[i]);
		if (! same_object) {
			this.mouse_over[i].on_mouse_out();
		}
	}

	this.mouse_over = [cell_view];
};

MainView.prototype.on_edit = function(cell_view) {
	for (i in this.edited) {
		var same_object = (cell_view===this.edited[i]);
		if (! same_object) {
			this.edited[i].on_cancel_edit();
		}
	}

	this.edited = [cell_view];
};

MainView.prototype.on_focus = function(cell_view) {
	var model = cell_view.get_model();
	this.cell_name = model.get_name();
	this.main_cell_bis = new CellViewBis(this.controller,model,this);
	this.trigger("focus", this.main_cell_bis.get_path());
	this.show();
};

MainView.prototype.on_drag_start = function() {
	this.dragging = true;
	this.trigger("dragging");
};

MainView.prototype.on_drag_stop = function() {
	this.dragging = false;
	this.trigger("dragging");
};

return MainView;

})
