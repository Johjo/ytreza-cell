include('cellnote.view.cell_event', [], function() {
	function on_click_cell(cell_view) {
		var e = new EventCellClick(cell_view);
		return e.get_event();
	}

	function on_click_edit(cell_view) {
		var e = new EventClickEdit(cell_view);
		return e.get_event();
	}


	function Event(cell_view) {
		this.cell_view = cell_view;
	}

	Event.prototype.on_event = function(event) {
		throw "not implemented";
	};

	Event.prototype.can = function(event) {
		return false;
	};

	Event.prototype.get_event = function() {
		return function(event) {
			if (!event.done) {
				event.done = true;
				if (this.can(event)) {
					var res = this.on_event(event);
					if (typeof res !== "undefined") {
						return res;
					}
				}
			} else {
				return true;
			}
		}.bind(this);
	};

	function EventCellClick(cell_view) {
		Event.call(this, cell_view);
	}

	EventCellClick.prototype = new Event;
	EventCellClick.prototype.can = function(first_argument) {return true;}

	EventCellClick.prototype.on_event = function(event) {
		if (event.ctrlKey) {
			this.cell_view.add_to_selection();
		} else {
			this.cell_view.select();
		}
	};

	function EventClickEdit(cell_view) {
		Event.call(this, cell_view);
	}

	EventClickEdit.prototype = new Event;
	EventClickEdit.prototype.can = function(first_argument) {return true;}

	EventClickEdit.prototype.on_event = function(event) {
		this.cell_view.edit();
	};

	return {
		on_click_cell : on_click_cell,
		on_click_edit : on_click_edit
	}
})