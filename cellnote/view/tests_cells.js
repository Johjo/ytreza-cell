var deps = ['cellnote.view.cell_action', 'cellnote.view.cell',
'cellnote.models.localefactory', 'cellnote.controller.cell', 'tools.tools','cellnote.view.main_view']

include("cellnote.view.tests_cells", deps, function(Action, CellView, ModelsManager, Controller, Tools, MainView) {
	module('View - CellView',{
		setup: function() {
		}
	});

	QUnit.test("test get_position", function( assert ) {
		var controller = new Controller();
		controller.set_model_manager(new ModelsManager(new Tools.MockStorage()));
		var main_view = new MainView(controller);

		var add_column = new Action.AddColumn(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);


		add_column.exec(cv1);
		add_column.exec(cv1);

		var cv2 = cv1.get_child(1,1);
		var pos = cv2.get_position();

		assert.equal(cv2.model.get_name(), "c2");
		assert.equal(pos.x,1, "x = 1");
		assert.equal(pos.y,1, "y = 1");
	});


	QUnit.test("test get_child", function( assert ) {
		var controller = new Controller();
		controller.set_model_manager(new ModelsManager(new Tools.MockStorage()));
		var main_view = new MainView(controller);

		var add_column = new Action.AddColumn(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);

		add_column.exec(cv1);
		add_column.exec(cv1);

		var cv2 = cv1.get_child(1,1);

		assert.equal(cv2.model.get_name(), "c2");
		assert.equal(cv2.parent.model.get_name(), "c1");
	});

	QUnit.test("bug 1 - has_brother", function( assert ) {
		var controller = new Controller();
		controller.set_model_manager(new ModelsManager(new Tools.MockStorage()));
		var main_view = new MainView(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);
		cm1.table.resize(2,2);

		var cv2 = cv1.get_child(1,1);
		assert.equal(cv2.model.get_name(), "", "1.1 : no name");
		assert.equal(cv2.model.x, 1, "1.2 : x = 1");
		assert.equal(cv2.model.y, 1, "1.2 : y = 1");

		console.log("here");
		assert.ok(cv2.has_brother(1,2), "2 brother exists");
		assert.ok(! cv2.has_brother(0,1), "3 brother does not exist");
	});


	return {}
})