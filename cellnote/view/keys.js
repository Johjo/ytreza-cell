include('cellnote.view.keys', function() {
	function Keys(element_listening, main_view) {
		riot.observable(this);

		this.cursor = main_view.cursor;

		var my_scope = this;
		var option_default = {
			"this" : my_scope,
			"prevent_default" : true
		}

		this.listener = new window.keypress.Listener(element_listening, option_default);

/*		this.listener.register_combo({
			keys: "ctrl left",
			on_keydown: function () {
				this.trigger("on_key", {key:"controle + left"});
			},
			is_exclusive : false
		})*/

		this.listener.simple_combo("left", function(event) {this.cursor.go_left(event.ctrlKey);});
		this.listener.simple_combo("right", function(event) {this.cursor.go_right(event.ctrlKey);});
		this.listener.simple_combo("up", function(event)  {this.cursor.go_up(event.ctrlKey);});
		this.listener.simple_combo("down", function(event) {this.cursor.go_down(event.ctrlKey);});

		this.listener.simple_combo("ctrl left", function(event) {console.log(event);this.trigger("on_key", {key:"controle + left"});});
	}

	return Keys;
})