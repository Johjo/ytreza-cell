var deps = ['cellnote.view.cell_action', 'cellnote.models.models', 'cellnote.view.cell',
'cellnote.controller.cell', 'tools.tools','cellnote.view.main_view']

include("tests_cell_action", deps, function(Action, Model, CellView, Controller, Tools, MainView) {
	var controller;
	var storage;

	module('View - Cell Action',{
		setup: function() {
			controller = new Controller();
			storage = Tools.MockStorage();
			controller.set_model_manager(new Model.ModelsManagerLocale(storage));
		}
	});

	QUnit.test("test add column", function( assert ) {
		storage.clear();
		var main_view = new MainView(controller);
		var add_column = new Action.AddColumn(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);

		add_column.exec(cv1);
		var cm2 = cm1.get_child(1,1);
		assert.equal(cm2.get_name(), "c2", "c2 -> name is c2");
		assert.equal(cm2.x, 1, "c2 -> x = 1");
		assert.equal(cm2.y, 1, "c2 -> y = 1");
		assert.equal(cm2.cm_parent, cm1, "c2 -> parent is cm1");

		add_column.exec(cv1);
		var cm3 = cm1.get_child(2,1);
		assert.equal(cm3.get_name(), "c3", "c3 -> name is c3");
		assert.equal(cm3.x, 2, "c3 -> x = 1");
		assert.equal(cm3.y, 1, "c3 -> y = 2");
		assert.equal(cm3.cm_parent, cm1, "c3 -> parent is cm1");
	});

	QUnit.test("test add cell to parent", function( assert ) {
		storage.clear();
		var main_view = new MainView(controller);

		var add_column = new Action.AddColumn(controller);
		var add_cell_to_parent = new Action.AddCellToParent(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);

		add_column.exec(cv1);
		add_column.exec(cv1);

		var cm2 = cm1.get_child(1,1);
		var cm3 = cm1.get_child(2,1);

		assert.equal(cm2.get_name(), "c2", "name = c2");
		assert.equal(cm2.x, 1, "x = 1");
		assert.equal(cm2.y, 1, "y = 1");
		assert.equal(cm3.get_name(), 'c3', "name = c3");
		assert.equal(cm3.x, 2, "x = 2");
		assert.equal(cm3.y, 1, "y = 1");


		var cv2 = cv1.get_child(1,1);

		add_cell_to_parent.exec(cv2);

		var cm4 = cm1.get_child(1,2);
		assert.equal(cm4.get_name(), "c4", "(1,2) => c4");
		assert.equal(cm4.x, 1, "x = 1");
		assert.equal(cm4.y, 2, "y = 2");


		add_cell_to_parent.exec(cv2);
		var cm5 = cm1.get_child(1,3);
		assert.equal(cm5.get_name(), "c5", "(1,3) => c5");
		assert.equal(cm5.x, 1, "x = 1");
		assert.equal(cm5.y, 3, "y = 3");


		add_cell_to_parent.exec(cv2);
		var cm6 = cm1.get_child(1,4);
		assert.equal(cm6.get_name(), "c6", "(1,4) => c6");
		assert.equal(cm6.x, 1, "x = 1");
		assert.equal(cm6.y, 4, "y = 4");
	});

	QUnit.test("bug - add child, column, row and update content doesn't work", function( assert ) {
		storage.clear();
		var main_view = new MainView(controller);

		var add_column = new Action.AddColumn(controller);
		var add_cell_to_parent = new Action.AddCellToParent(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);

		controller.create_child(cm1,1,1,"down");
		controller.add_column(cm1);
		controller.add_cell(cm1,2);

		var cm5 = cm1.get_child(1,2);

		assert.equal(cm5.get_name(), "", "cm5 -> no name");
		assert.equal(cm5.get_content(), "", "cm5 -> no content");
		console.log("before");
		controller.update_content_cell(cm5, "content cm5")
		console.log("after");

		assert.equal(cm5.get_name(), "c5", "cm5 -> name is 'c5'");
		assert.equal(cm5.get_content(), "content cm5", "cm5 -> content is 'content cm5'");
		assert.equal(cm1.get_child(1,2).get_name(), "c5", "cm5 -> name is 'c5'");
		assert.equal(cm1.get_child(1,2).get_content(), "content cm5", "cm5 -> content is 'content cm5'");
	});

	QUnit.test("bug - add child, column, column and get 3 columns", function( assert ) {
		storage.clear();
		var main_view = new MainView(controller);

		var add_column = new Action.AddColumn(controller);

		var cm1 = controller.create_cell();
		var cv1 = new CellView(controller,cm1,null,main_view);

		controller.create_child(cm1,1,1,"down");
		assert.ok(cm1.get_child(1,1));

		assert.throws(function() {
			cm1.get_child(2,1);
		}, {message: "child at (2, 1) doesn't exist", "name": "CellException"}, "exception");

		console.log("here");
		add_column.exec(cv1);
		assert.ok(cm1.get_child(1,1));
		assert.ok(cm1.get_child(2,1));

		assert.throws(function() {
			cm1.get_child(3,1);
		}, {message: "child at (3, 1) doesn't exist", "name": "CellException"}, "exception");


		add_column.exec(cv1);

		assert.ok(cm1.get_child(1,1));
		assert.ok(cm1.get_child(2,1));
		assert.ok(cm1.get_child(3,1));

		assert.throws(function() {
			cm1.get_child(4,1);
		}, {message: "child at (4, 1) doesn't exist", "name": "CellException"}, "exception");

	});

	return {}
})