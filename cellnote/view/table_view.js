include('cellnote.view.table_view', function() {
	function TableView(cell_model, path) {
		riot.observable(this);

		this.cell_model = cell_model;
		this.children = [];
		this.path = path;

		this.cell_model.on("update_child", function(detail) {
			this.build_children();
			this.trigger("update_child")
		}.bind(this));

		this.build_children();
	}

	TableView.prototype.build_children = function() {
		this.children = [];

		var row_count = this.cell_model.get_row_count();
		var col_count = this.cell_model.get_col_count();
		var children = this.cell_model.get_children();

		var button_added = {};
		var row = [];

		if (row_count > 0) {
			for (var i = 0; i < row_count; i++) {	/* row */
				var y = i + 1;
				row = [];
				for (var j = 0; j < col_count; j++) { /* col */
					var x = j + 1;
					if (children[i] && children[i][j]) {
						row.push(this.create_child("cell", children[i][j]));
					}
				};
				this.children.push({row:row});
			};
		}	
	};

	TableView.prototype.create_child = function(type, data) {
		var path_el = [];

		var name = data.name == "" ? "new" : data.name;

		if (this.path !== "") {path_el.push(this.path)};
		if (name !== "") {path_el.push(name)};

		var path = path_el.join(".");

		return {
			path: path,
			name: name,
			type: type,
			model: data,
			x : data.x,
			y : data.y
		}
	};



	return TableView;
})
