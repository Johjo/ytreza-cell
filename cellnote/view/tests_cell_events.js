var deps = ['cellnote.view.cell_action', 'cellnote.models.models', 'cellnote.view.cell',
'cellnote.controller.cell', 'tools.tools','cellnote.view.main_view', 'cellnote.view.cell_event']

include("tests_cell_events", deps, function(Action, Model, CellView, Controller, Tools, MainView, Event) {
	module('View - Cell Events',{
		setup: function() {
		}
	});

	QUnit.test("test cell click", function( assert ) {
		var controller = new Controller();
		controller.set_model_manager(new Model.ModelsManagerLocale(new Tools.MockStorage()));
		var main_view = new MainView(controller);

		var cv1 = main_view.create_cell(controller.create_cell());
		var cv2 = main_view.create_cell(controller.create_cell());

		/* initial state */		
		assert.equal(cv1.selected,false, "initial : cell 1 is not selected");
		assert.equal(cv2.selected,false, "initial : cell 2 is not selected");


		var ev1 = Event.on_click_cell(cv1);
		var ev2 = Event.on_click_cell(cv2);

		var event={};

		ev1(event);

		assert.ok(cv1.selected,"cell 1 is selected");
		assert.equal(cv2.selected,false, "cell 2 is not selected");
		assert.equal(main_view.cursor.cv_current, cv1, "cv1 is current cell");


		event={}
		ev2(event);

		assert.equal(cv1.selected,false, "cell 1 is not selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv2, "cv2 is current cell");

		event={}
		event.ctrlKey = true;
		ev1(event);

		assert.ok(cv1.selected,"cell 1 is selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv1, "cv1 is current cell");

		event={}
		ev2(event);

		assert.equal(cv1.selected,false, "cell 1 is not selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv2, "cv2 is current cell");


		event={}
		event.ctrlKey = true;
		ev1(event);

		assert.ok(cv1.selected,"cell 1 is selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv1, "cv1 is current cell");

		event={}
		ev2(event);

		assert.equal(cv1.selected,false, "cell 1 is not selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv2, "cv2 is current cell");
	});

	QUnit.test("test cell click bug 1", function( assert ) {
		var controller = new Controller();
		controller.set_model_manager(new Model.ModelsManagerLocale(new Tools.MockStorage()));
		var main_view = new MainView(controller);

		var cv1 = main_view.create_cell(controller.create_cell());
		var cv2 = main_view.create_cell(controller.create_cell());

		/* initial state */		
		assert.equal(cv1.selected,false, "initial : cell 1 is not selected");
		assert.equal(cv2.selected,false, "initial : cell 2 is not selected");


		var ev1 = Event.on_click_cell(cv1);
		var ev2 = Event.on_click_cell(cv2);

		var event={};

		/* bug 1 */
		event={}
		ev1(event);
		assert.equal(main_view.cells_selected.length,1, "one cell selected");
		event={}
		event.ctrlKey = true;
		ev2(event);
		assert.equal(main_view.cells_selected.length,2, "two cells selected");
		event={}
		ev1(event);
		assert.equal(main_view.cells_selected.length,1, "one cell selected");
		event={}
		ev2(event);
		assert.equal(main_view.cells_selected.length,1, "one cell selected");
		
		assert.equal(cv1.selected,false, "cell 1 is not selected");
		assert.ok(cv2.selected,"cell 2 is selected");
		assert.equal(main_view.cursor.cv_current, cv2, "cv2 is current cell");
	});
	return {}
})