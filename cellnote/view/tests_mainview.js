var deps = ['cellnote.view.tools', 'cellnote.view.main_view', 'cellnote.controller.cell','tools.tools','cellnote.models.models'];

include("tests_mainview", deps, function(ViewTools, MainView,Controller,Tools,Model) {
	var controller;
	var storage;
	var manager;

	module('View - MainView',{
		setup: function() {
			controller = new Controller();
			storage = new Tools.MockStorage();
			manager = new Model.ModelsManagerLocale(storage);

			controller.set_model_manager(manager);
		}
	});

	QUnit.test("test analyze_path", function( assert ) {
		assert.equal(ViewTools.analyze_path("c0").name, "c0", "name is ok");
		assert.equal(ViewTools.analyze_path("c0").parent, "", "parent is ok");

		assert.equal(ViewTools.analyze_path("c0.c1").name, "c1", "name is ok");
		assert.equal(ViewTools.analyze_path("c0.c1").parent, "c0", "parent is ok");

		assert.equal(ViewTools.analyze_path("c0.c1.c2").name, "c2", "name is ok");
		assert.equal(ViewTools.analyze_path("c0.c1.c2").parent, "c0.c1", "parent is ok");

		assert.equal(ViewTools.analyze_path(".").name, "", "name is ok");
		assert.equal(ViewTools.analyze_path(".").parent, "", "parent is ok");

		assert.equal(ViewTools.analyze_path("").name, "", "name is ok");
		assert.equal(ViewTools.analyze_path("").parent, "", "parent is ok");

		assert.equal(ViewTools.analyze_path("c0.").name, "", "name is ok");
		assert.equal(ViewTools.analyze_path("c0.").parent, "c0", "parent is ok");
	});

	QUnit.test("test create_cell", function( assert ) {
		var main_view = new MainView(controller);
		var cm = manager.create_cell();
		assert.equal(cm.get_parent(), null, "get_parent is null");

		var cv = main_view.create_cell(cm);

		assert.equal(cv.model.get_parent(), null, "get_parent is null");
	});

	QUnit.test("test get_cell_key", function( assert ) {
		var main_view = new MainView(controller);
		var cm1 = manager.create_cell();

		assert.equal(main_view.get_cell_key(cm1), ".c1(1,1)");
		var cm2 = manager.create_cell();
		cm1.insert_child(cm2, 2, 2, "down");

		assert.equal(main_view.get_cell_key(cm1.get_child(1,1)), "c1.(1,1)");
		assert.equal(main_view.get_cell_key(cm1.get_child(1,2)), "c1.(1,2)");
		assert.equal(main_view.get_cell_key(cm1.get_child(2,1)), "c1.(2,1)");
		assert.equal(main_view.get_cell_key(cm1.get_child(2,2)), "c1.c2(2,2)");
	});

	QUnit.test("test get_cell_from_model", function( assert ) {
		var main_view = new MainView(controller);

		var cm = new Model.CellModel("");
		var cv = main_view.get_cell_from_model(cm);
		assert.equal(cv.model.get_name(), "");
		assert.equal(cv.model.get_parent(), null, "get_parent is null");
	});

	return {}
})