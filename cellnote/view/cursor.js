include('cellnote.view.cursor', [], function() {

	function Cursor() {
		this.cv_current = null;
		this.pos={x:0,y:0};
	}

	Cursor.prototype.set_position = function(cv_current) {
		this.cv_current = cv_current;
		this.pos=this.cv_current.get_position();
		this.cv_current.select();
	};

	Cursor.prototype.get_cell = function() {
		return this.cv_current;
	};

	Cursor.prototype.go_right = function(same_level) {
		this.go_right_or_down(1,0,same_level);
	};

	Cursor.prototype.go_down = function(same_level) {
		this.go_right_or_down(0,1,same_level);
	};

	Cursor.prototype.go_right_or_down = function(x_step,y_step,same_level) {
		if (!same_level && this.cv_current.has_children()) {
			this.set_position(this.cv_current.get_child(1,1));
		} else {
			/* take father or great father or ...*/
			var cv = this.cv_current;
			while(cv) {
				var pos = cv.get_position();
				if (cv.has_brother(pos.x+x_step, pos.y+y_step)) {
					this.set_position(cv.get_brother(pos.x+x_step, pos.y+y_step));
					break;
				} else if (same_level) {
					break;
				} else {
					cv = cv.parent;
				}
			}
		}
	};

	function get_bro_or_child(cv_bro, x_step, y_step) {
		if (cv_bro.has_children()) {
			var x = (x_step == 1 ? "last" : 1);
			var y = (y_step == 1 ? "last" : 1);

			return get_bro_or_child(cv_bro.get_child(x,y), x_step, y_step);
		} else {
			return cv_bro;
		}
	}

	Cursor.prototype.go_left = function(same_level) {
		this.go_up_or_left(1,0,same_level);
	};

	Cursor.prototype.go_up = function(same_level) {
		this.go_up_or_left(0,1,same_level);
	};

	Cursor.prototype.go_up_or_left = function(x_step,y_step,same_level) {
		var pos = this.pos;

		if (this.cv_current.has_brother(pos.x - x_step,pos.y - y_step)) {
			var cv_bro = this.cv_current.get_brother(pos.x - x_step ,pos.y - y_step);
			if (!same_level) {
				cv_bro = get_bro_or_child(cv_bro, x_step, y_step);
			}
			this.set_position(cv_bro);

		} else if (this.cv_current.parent) {
			this.set_position(this.cv_current.parent);
		}
	};


	return Cursor
});