include("cellnote.view.test_table_view", ['cellnote.view.table_view', 'cellnote.models.cell'], function(TableView,CellModel) {
	module('View - TableView',{
		setup: function() {
		}
	});

	QUnit.test("Test 1", function( assert ) {

		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);
		var table = new TableView(c0,"c0");


		assert.equal(table.children.length,2, "length of children is 2");
		var child00 = table.children[0].row[0]
		var child10 = table.children[0].row[1]
		var child01 = table.children[1].row[0]
		var child11 = table.children[1].row[1]

		assert.equal(child00.type, "cell", "type in 0,0 is cell");
		assert.equal(child00.name, "c1", "name in 0,0 is c1");
		assert.equal(child00.path, "c0.c1", "path in 0,0 is c0.c1");
		assert.equal(child00.x, 1, "x in 0,0 is 1");
		assert.equal(child00.y, 1, "y in 0,0 is 1");
		assert.equal(child00.model.get_parent(), c0, "cell's father in 0,0 is c0");


		assert.equal(child10.type, "cell", "type in 0,1 is cell");
		assert.equal(child10.name, "new", "name in 0,1 is 'new'");
		assert.equal(child10.path, "c0.new", "path in 0,1 is c0.new");
		assert.equal(child10.x, 2, "x in 0,1 is 2");
		assert.equal(child10.y, 1, "y in 0,1 is 1");

		assert.equal(child01.type, "cell", "type in 1,0 is cell");
		assert.equal(child01.name, "c2", "name in 1,0 is c2");
		assert.equal(child01.path, "c0.c2", "path in 1,0 is c0.c2");
		assert.equal(child01.x, 1, "x in 1,0 is 1");
		assert.equal(child01.y, 2, "y in 1,0 is 2");




		assert.equal(child11.type, "cell", "type in 1,1 is cell");
		assert.equal(child11.name, "c3", "name in 1,1 is c3");
		assert.equal(child11.path, "c0.c3", "path in 1,1 is c0.c3");
		assert.equal(child11.x, 2, "x in 1,1 is 2");
		assert.equal(child11.y, 2, "y in 1,1 is 2");
	});

	QUnit.test("Test 2", function( assert ) {

		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);
		var c4 = new CellModel("c4");
		var table = new TableView(c0,"c0");

		c0.insert_child(c4,2,1,"down");

		assert.equal(table.children.length,3, "length of children is 3");

		var child00 = table.children[0].row[0]
		var child10 = table.children[0].row[1]
		var child01 = table.children[1].row[0]
		var child11 = table.children[1].row[1]
		var child12 = table.children[2].row[1]

		assert.equal(child00.type, "cell", "type in 0,0 is cell");
		assert.equal(child00.name, "c1", "name in 0,0 is c1");
		assert.equal(child00.path, "c0.c1", "path in 0,0 is c0.c1");
		assert.equal(child00.x, 1, "x in 0,0 is 1");
		assert.equal(child00.y, 1, "y in 0,0 is 1");

		assert.equal(child10.type, "cell", "type in 0,1 is cell");
		assert.equal(child10.name, "c4", "name in 0,1 is c4");
		assert.equal(child10.path, "c0.c4", "path in 0,1 is c0.c4");
		assert.equal(child10.x, 2, "x in 0,1 is 2");
		assert.equal(child10.y, 1, "y in 0,1 is 1");

		assert.equal(child01.type, "cell", "type in 1,0 is cell");
		assert.equal(child01.name, "c2", "name in 1,0 is c2");
		assert.equal(child01.path, "c0.c2", "path in 1,0 is c0.c2");
		assert.equal(child01.x, 1, "x in 1,0 is 1");
		assert.equal(child01.y, 2, "y in 1,0 is 2");

		assert.equal(child11.type, "cell", "type in 1,1 is cell");
		assert.equal(child11.name, "new", "name in 1,1 is 'new'");
		assert.equal(child11.path, "c0.new", "path in 1,1 is c0.new");
		assert.equal(child11.x, 2, "x in 1,1 is 2");
		assert.equal(child11.y, 2, "y in 1,1 is 2");

		assert.equal(child12.type, "cell", "type in 2,1 is cell");
		assert.equal(child12.name, "c3", "name in 2,1 is c3");
		assert.equal(child12.path, "c0.c3", "path in 2,1 is c0.c3");
		assert.equal(child12.x, 2, "x in 2,1 is 2");
		assert.equal(child12.y, 3, "y in 2,1 is 2");

		assert.ok(table);
		assert.ok(c0);
	});

	QUnit.test("Test 3 - Bug", function( assert ) {

		var c0 = new CellModel();
		var table = new TableView(c0,"c0");


		c0.insert_child(new CellModel("c1"),1,1,"down");
		c0.insert_child(new CellModel("c2"),1,2,"down");
		c0.insert_child(new CellModel("c2"),2,1,"down");

		assert.ok(c0);
	});


	return {}
})