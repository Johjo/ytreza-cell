include('cellnote.view.view', function() {
	function View(controller) {
		this.controller = controller;
	}

	View.prototype.init = function() {
		this.controller.add_view(this);
	};

	View.prototype.show = function() {
		throw "view show not defined";
	};

	return View;
})