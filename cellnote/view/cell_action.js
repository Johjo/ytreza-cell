include('cellnote.view.cell_action', [], function() {
	function Action(controller) {
		this.controller = controller;
	}

	Action.prototype.exec = function() {
		throw "not implemented";
	};

	/* action : edit a cell */

	function ActionEdit() {
		Action.call(this);
	}

	ActionEdit.prototype = new Action;

	ActionEdit.prototype.exec = function(cell_view) {
		cell_view.edit();
	};


	/* action : add a column */

	function ActionAddColumn(controller) {
		Action.call(this,controller);
	}

	ActionAddColumn.prototype = new Action;

	ActionAddColumn.prototype.exec = function(cell_view) {
		this.controller.add_column(cell_view.model);
	};


	/* action : add a cell */

	function ActionAddCellToParent(controller) {
		Action.call(this,controller);
	}

	ActionAddCellToParent.prototype = new Action;

	ActionAddCellToParent.prototype.exec = function(cell_view) {
		var x = cell_view.get_position().x;

		var cell_model = cell_view.parent.get_model_for_update();
		cell_view.controller.add_cell(cell_model,x);
	};

	return {
		Edit : ActionEdit,
		AddColumn : ActionAddColumn,
		AddCellToParent : ActionAddCellToParent
	}
})