var LAYOUT_CELL = 1;
var LAYOUT_LIST_HORIZONTAL = 2;
var LAYOUT_LIST_VERTICAL = 3;
var LAYOUT_TABLE = 4;

var deps = ['tools.table']

include('cellnote.models.cell', deps, function(Table) {

	function CellModel(data, cm_parent, x, y, factory) {
		riot.observable(this);

		this.factory = factory;
		this.cm_parent = cm_parent;
		this.x = x ? x : 1;
		this.y = y ? y : 1;

		this.table = new Table(function(data){
			data.data = this.create_empty_child(data.x, data.y)
		}.bind(this));

		if (typeof data == "string") {
			if (factory) {
				this.data = factory.get_cell_data(data);
			} else {
				this.data = {name: data, content: "", child: [], archived: false};	
			}
			this.name = data;
		} else if (data) {
			this.data = data;
		} else {
			this.data = {name: "", content: "", child: [], archived: false};
		}

		/* default value */
		if (typeof this.data.name === "undefined") {this.data.name = "";}
		if (typeof this.data.content === "undefined") {this.data.content = "";}
		if (typeof this.data.children === "undefined") {this.data.children = []};
		if (typeof this.data.archived === "undefined") {this.data.archived = false};
		if (typeof this.data.layout === "undefined") {this.data.layout = LAYOUT_LIST_VERTICAL};
		if (typeof this.data.row_count === "undefined") {this.data.row_count = 0};
		if (typeof this.data.col_count === "undefined") {this.data.col_count = 0};

		/* build child from data */
		for (var i = 0; i < this.data.children.length; i++) {
			for (var j = 0; j < this.data.children[i].length; j++) {
				this.data.row_count = Math.max(this.data.row_count, i+1);
				this.data.col_count = Math.max(this.data.col_count, j+1);
			}
		}

		this.table.resize(this.data.col_count, this.data.row_count);

		for (var i = 0; i < this.data.row_count; i++) {
			for (var j = 0; j < this.data.col_count; j++) {
				if (this.data.children[i][j]) {
					var x = j+1;
					var y = i+1;
					this.table.at(x,y).data = new CellModel(this.data.children[i][j], this, j+1, i+1, this.factory);
				}
			};
		};
	}

	CellModel.prototype.save = function() {
		if (this.factory) {
			this.factory.save_cell(this);
		} else {
			console.warn("factory is not defined");
		}
	};


	CellModel.prototype.get_parent = function() {
		return this.cm_parent;
	};

	CellModel.prototype.get_row_count = function() {
		return this.table.row_count;
	};

	CellModel.prototype.get_col_count = function() {
		return this.table.col_count;
	};

	CellModel.prototype.insert_child = function(cm, x, y, direction) {
		cm.cm_parent = this;
		cm.factory = this.factory
		cm.x = x;
		cm.y = y;
		cm.save();

		/* if the last cell of the row or the column is empty, it's replaced by the previous, else, another row or col will be added*/
		var redim = true;

		switch(direction) {
			case "right":
				redim = redim && x <= this.table.col_count;
				redim = redim && this.table.at("last", y) != null;
				redim = redim && this.table.at("last", y).data.name != "";
				break;
			case "down":
				redim = redim && x <= this.table.row_count;
				redim = redim && this.table.at(x,"last") != null;
				redim = redim && this.table.at(x,"last").data.name != "";

				break;
		}

		/* if the table is not large enough, resize the table*/
		this.table.resize(Math.max(x, this.table.col_count), Math.max(y, this.table.row_count));

		this.table.insert(cm,x,y,direction,redim,function(data) {
			var cm_child = data.data;
			cm_child.x = data.x;
			cm_child.y = data.y;
		});

		this.trigger("add_child");
		this.trigger("update_child");

		return cm;
	};

	CellModel.prototype.add_child_in_column = function(child, x) {
		var y = this.table.row_count;

		while (y > 0 && this.get_child(x,y).get_name() == "")  {
			y--;
		}

		this.insert_child(child, x, y + 1, "down");
	}

	CellModel.prototype.get_number_of_children = function() {
		return this.data.children.length + 1;
	};

	/* Compte le nombre de cellules enfants archivées */
	CellModel.prototype.get_number_of_children_not_archived = function() {
		var number = 0;
		for (var i = 0; i < this.data.children.length; i++) {
			var child = this.get_child(i + 1);
			if (! child.is_archived()) {
				number ++;
			}
		};

		return number;
	};

	CellModel.prototype.get_name = function() {
		return this.data.name;
	};

	CellModel.prototype.set_name = function(name) {
		if (this.data.name == "")  {
			this.data.name = name;
			this.trigger("update", {field : 'name'});
		} else {
			throw new CellException("name is already defined");
		}
	};

	CellModel.prototype.get_content = function() {
		return this.data.content;
	}

	CellModel.prototype.set_content = function(content) {
		this.data.content = content;
		this.trigger("update", {field : 'content'});
	};

	CellModel.prototype.is_archived = function() {
		return this.data.archived;
	};

	CellModel.prototype.set_archived = function(archived) {
		this.data.archived = archived;
		this.trigger("update", {field : 'archived'});
	};

	CellModel.prototype.get_layout = function() {
		return this.data.layout;
	};

	CellModel.prototype.set_layout = function(layout) {
		switch(layout) {
			case LAYOUT_LIST_HORIZONTAL :
			case LAYOUT_LIST_VERTICAL: this.data.layout = layout ; break;
			default: throw new CellException("layout doesn't exist");
		}

		this.trigger("update", {field : 'layout'});

		return this.data.layout;
	};

	CellModel.prototype.remove_child = function(cm_child, direction) {
		var x = cm_child.x;
		var y = cm_child.y;

		if (this.table.at(x, y)) {
			if (this.table.at(x, y).data == cm_child) {
				var done = this.table.remove(x,y,direction, function(data) {
					var cm_child = data.data;
					cm_child.x = data.x;
					cm_child.y = data.y;
				});
				if (done) {this.trigger("remove_child");};
			}
		}
	};

	CellModel.prototype.create_child = function() {
		var cell = this.factory.create_cell();
		var cell_name = cell.get_name();

		var offset = this.data.children.push(cell.get_name());

		this.save();

		this.trigger("add_child", {name : cell_name, offset: offset + 1});

		return cell;
	};

	CellModel.prototype.create_empty_child = function(x,y) {
		var cell = new CellModel("", this, x, y, this.factory);

		this.data.row_count = Math.max(this.data.row_count, y);
		this.data.col_count = Math.max(this.data.row_count, x);

		cell.on("update", function(detail) {
			if (detail.field=="name") {
				this.save();
			}
		}.bind(this));

		return cell
	};

	CellModel.prototype.get_data = function() {
		var children = this.get_children();
		this.data.children = [];
		for (var i = 0; i < children.length; i++) {
			this.data.children.push([]);
			for (var j = 0; j < children[i].length; j++) {
				this.data.children[i].push(children[i][j].get_name())
			};
		};

		return this.data;
	};

	CellModel.prototype.get_children_names = function() {
		var names = [];

		for (var i = 0; i < this.data.children.length; i++) {
			names.push(this.data.children[i]);
		};

		return names;
	};

	CellModel.prototype.get_children = function() {
		var children = [];

		for (var y = 1; y <= this.get_row_count() ; y++) {
			children.push([]);
			for (var x = 1; x <= this.get_col_count(); x++) {
				children[y-1].push(this.get_child(x,y));
			};
		};

		return children;
	};

	CellModel.prototype.get_unarchived_children	 = function() {
		var names = this.data.children;
		var children = [];
		for (var i = 0; i < names.length; i++) {
			var child = this.factory.get_cell(names[i]);
			if (! child.is_archived()) {
				children.push(child);
			}
		};

		return children;
	};


	CellModel.prototype.get_child = function(x, y) {
		var data = this.table.at(x, y);

		if (data) {
			return data.data;
		}


		throw new CellException("child at (" + x +", "+ y + ") doesn't exist");
	}

	CellModel.prototype.has_children = function() {
		return this.table.row_count > 0 &&  this.table.col_count > 0;
	};

	function CellException(message) {
		this.message = message;
		this.name = "CellException";
	}

	CellException.prototype.toString = function() {
		return this.message;
	};

	return CellModel;

})




