var ModelsManagerLocale;

include("test_cell", ['localefactory'], function(ModelsManagerLocale) {
	return {}
})

	var factory;

	module('Model Cell',{
		setup: function() {
			factory = new ModelsManagerLocale({});
		}
	});

	QUnit.test("Cell Test 1", function( assert ) {
		localStorage.clear();

		var c0 = factory.get_cell("c0");

		assert.equal(c0.get_name(), "c0", "c0's name is c0");
		assert.ok(!c0.get_children()[0], "c0 doesn't have children");


		var c1 = c0.create_child();

		assert.equal(JSON.stringify(c0.get_children()), JSON.stringify(["c1"]));

		assert.equal(c1.get_name(), "c1", "c1's name is c1");
		assert.ok(c0.get_children()[0], "c0 has one child");
		assert.ok(!c0.get_children()[1], "c0 has one child");

		var c2 = c1.create_child();

		assert.equal(c2.get_name(), "c2", "c2's name is c2");

		var c2bis = c1.get_child(1);
		assert.equal(c2bis.get_name(), c2.get_name());
	});


	QUnit.test("Cell Test 2", function( assert ) {
		localStorage.clear();

		var c0 = factory.get_cell("c0");

		assert.equal(c0.get_name(), "c0", "c0's name is c0");
		assert.ok(!c0.get_children()[0], "c0 doesn't have children");


		var c1 = c0.create_child();
		var c2 = c0.create_child();



		assert.equal(JSON.stringify(c0.get_children()), JSON.stringify(["c1","c2"]));

	});
