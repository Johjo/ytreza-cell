include('cellnote.models.localefactory', ['cellnote.models.models_factory', 'cellnote.models.cell'], function(ModelsManager, CellModel) {
	function ModelsManagerLocale(storage) {
		if (!storage) {
			this.storage = localStorage;
		} else {
			this.storage = storage;
		}
		ModelsManager.call(this);

		if (this.storage["last_cell"]) {
			this.last_cell = parseInt(this.storage.getItem("last_cell"));
		} else {
			this.last_cell = 0;
		}
		this.cells = {};
	}

	ModelsManagerLocale.prototype = new ModelsManager;

	ModelsManagerLocale.prototype.get_cell = function(name) {
		if (name=="") {
			return null;
		}

		if (! this.cells[name]) {
			var data;
			if (!this.storage[name]) {
				data = {name: name};
			} else {
				data = JSON.parse(this.storage[name]);
			}
			this.cells[name] = new CellModel(name,null,1,1,this);
		}

		return this.cells[name];
	};

	ModelsManagerLocale.prototype.get_cell_data = function(name) {
		if (name=="") {
			return {name: name};
		}

		var data;
		if (!this.storage[name]) {
			data = {name: name};
		} else {
			data = JSON.parse(this.storage[name]);
		}
		return data;
	};

	ModelsManagerLocale.prototype.save_cell = function(cell) {
		/* if it's a new cell, put a name */
		if (cell.get_name() == "") {
			cell.set_name(this.get_new_name());
		}

		if (! this.cells[cell.get_name()]) {
			this.cells[cell.get_name()] = cell;
		}
		this.storage[cell.get_name()] = JSON.stringify(cell.get_data());
	};

	ModelsManagerLocale.prototype.get_new_name = function() {
		this.last_cell = this.last_cell + 1;
		this.storage["last_cell"] = this.last_cell;

		var name  = "c" + this.last_cell

		return name;
	};

	ModelsManagerLocale.prototype.create_cell = function(cm_parent,x,y) {
		var name = this.get_new_name();

		var cell = new CellModel(name, cm_parent, x, y, this);
		this.save_cell(cell);
		return cell;
	};

	return ModelsManagerLocale;
})
