var deps = ['cellnote.models.localefactory', 'cellnote.models.cell'];

include('cellnote.models.models', deps, function(ModelsManagerLocale, CellModel) {
	return {
		ModelsManagerLocale : ModelsManagerLocale,
		CellModel : CellModel
	};
})
