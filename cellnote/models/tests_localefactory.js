var deps = ['cellnote.models.localefactory', 'tools.tools'];

include("tests_localefactory", deps, function(ModelsManagerLocale, Tools) {
	module('Model - Locale Factory',{
		setup: function() {
		}
	});

	QUnit.test("create_cell", function( assert ) {
		var storage = new Tools.MockStorage();
		var factory = new ModelsManagerLocale(storage);

		var cm1 = factory.create_cell();

		assert.equal(cm1.get_name(), "c1", "c1 -> name is c1");
		assert.equal(cm1.x, 1, "c1 -> x is 1");
		assert.equal(cm1.y, 1, "c1 -> y is 1");

		assert.equal(cm1.get_parent(), null, "c1 -> get_parent is null");
		assert.equal(cm1.factory, factory, "c1 -> factory is good");

		var cm2 = factory.create_cell(cm1);
		assert.equal(cm2.get_parent(), cm1, "c2 -> get_parent is cm1");
		assert.equal(cm2.factory, factory, "c2 -> factory is good");
	});

	QUnit.test("get_new_name", function( assert ) {
		var storage = new Tools.MockStorage();
		var factory = new ModelsManagerLocale(storage);

		assert.equal(factory.get_new_name(), "c1");
		assert.equal(factory.get_new_name(), "c2");
		assert.equal(factory.get_new_name(), "c3");
		assert.equal(factory.get_new_name(), "c4");
	});

	QUnit.test("BUG - cell.get_data", function( assert ) {
		var storage = sessionStorage;
		storage.clear();

		var data = {name:"c1", children:[["c2","c3"],["","c4"]]};
		storage["last_cell"] = 4;
		storage["c1"] = JSON.stringify(data);

		var factory = new ModelsManagerLocale(storage);

		var c1 = factory.get_cell("c1");
		var c5 = c1.get_child(1,2);

		assert.equal(c5.get_name(), "");

		c5.save();

		assert.equal(c5.get_name(), "c5");
		assert.equal(c1.get_child(1,2).get_name(), "c5");
	});

	return {}
})