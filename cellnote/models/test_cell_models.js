include("test_cell_models", ['cellnote.models.localefactory','cellnote.models.cell','tools.tools'], function(ModelsManagerLocale,CellModel,Tools) {
	var factory;
	var storage;

	module('Model - Cell',{
		setup: function() {
			storage = Tools.MockStorage();
			factory = new ModelsManagerLocale(storage);
		}
	});

	function convert(cell) {
		return {name:cell.get_name(), x:cell.x, y:cell.y};
	}


	QUnit.test("Cell Test 1", function( assert ) {
		storage.clear();

		var c0 = factory.get_cell("c0");

		assert.equal(c0.get_name(), "c0", "c0's name is c0");
		assert.ok(!c0.get_children()[0], "c0 doesn't have children");

		assert.equal(c0.get_row_count(), 0, "row count");
		assert.equal(c0.get_col_count(), 0, "col count");

		var c1 = factory.create_cell();
		c0.insert_child(c1, 1, 1, "down");
		assert.equal(c0.table.at(1,1).data.name, 'c1', "first child is c1");

		assert.equal(c0.get_row_count(), 1, "row count is 1");
		assert.equal(c0.get_col_count(), 1, "col count is 1");

		assert.equal(c0.table.at(1,1).data.name, "c1", "c1 is good");
		assert.equal(c0.table.at(1,1).data.x, 1, "c1 is good");
		assert.equal(c0.table.at(1,1).data.y, 1, "c1 is good");

		var c2 = factory.create_cell();
		c0.insert_child(c2, 1, 1 , "right");

		assert.equal(c0.get_row_count(), 1, "row count is 1");
		assert.equal(c0.get_col_count(), 2, "col count is 2");

		assert.equal(c0.table.at(2,1).data.name, "c1", "c1 is good");
		assert.equal(c0.table.at(2,1).data.x, 2, "c1 is good");
		assert.equal(c0.table.at(2,1).data.y, 1, "c1 is good");

		assert.equal(c0.table.at(1,1).data.name, "c2", "c2 is good");
		assert.equal(c0.table.at(1,1).data.x, 1, "c2 is good");
		assert.equal(c0.table.at(1,1).data.y, 1, "c2 is good");

		var c3 = factory.create_cell();
		c0.insert_child(c3, 1, 1 , "down");

		assert.equal(c0.get_row_count(), 2, "row count is 2");
		assert.equal(c0.get_col_count(), 2, "col count is 2");

		assert.equal(c0.table.at(2,1).data.name, "c1", "c1 is good");
		assert.equal(c0.table.at(2,1).data.x, 2, "c1 is good");
		assert.equal(c0.table.at(2,1).data.y, 1, "c1 is good");

		assert.equal(c0.table.at(1,2).data.name, "c2", "c2 is good");
		assert.equal(c0.table.at(1,2).data.x, 1, "c2 is good");
		assert.equal(c0.table.at(1,2).data.y, 2, "c2 is good");

		assert.equal(c0.table.at(1,1).data.name, "c3", "c3 is good");
		assert.equal(c0.table.at(1,1).data.x, 1, "c3 is good");
		assert.equal(c0.table.at(1,1).data.y, 1, "c3 is good");

		var c4 = factory.create_cell();
		c0.insert_child(c4,2,2,"down");

		assert.equal(c0.get_row_count(), 2, "row count is 2");
		assert.equal(c0.get_col_count(), 2, "col count is 2");

		var data = c0.table.at(2,1);
		var c1 = c0.table.at(2,1).data;
		assert.propEqual({name:c1.name, x:c1.x, y:c1.y}, {name:"c1", x:2, y:1}, "c1 is good");


		assert.equal(c0.table.at(1,2).data.name, "c2", "c2 is good");
		assert.equal(c0.table.at(1,2).data.x, 1, "c2 is good");
		assert.equal(c0.table.at(1,2).data.y, 2, "c2 is good");

		assert.equal(c0.table.at(1,1).data.name, "c3", "c3 is good");
		assert.equal(c0.table.at(1,1).data.x, 1, "c3 is good");
		assert.equal(c0.table.at(1,1).data.y, 1, "c3 is good");

		assert.equal(c0.table.at(2,2).data.name, "c4", "c4 is good");
		assert.equal(c0.table.at(2,2).data.x, 2, "c4 is good");
		assert.equal(c0.table.at(2,2).data.y, 2, "c4 is good");

		var c5 = factory.create_cell();
		c0.insert_child(c5,2,1,"down");

		assert.equal(c0.get_row_count(), 3, "row count is 3");
		assert.equal(c0.get_col_count(), 2, "col count is 2");

		assert.equal(c0.table.at(2,2).data.name, "c1", "c1 is good");
		assert.equal(c0.table.at(2,2).data.x, 2, "c1 is good");
		assert.equal(c0.table.at(2,2).data.y, 2, "c1 is good");

		assert.equal(c0.table.at(1,2).data.name, "c2", "c2 is good");
		assert.equal(c0.table.at(1,2).data.x, 1, "c2 is good");
		assert.equal(c0.table.at(1,2).data.y, 2, "c2 is good");

		assert.equal(c0.table.at(1,1).data.name, "c3", "c3 is good");
		assert.equal(c0.table.at(1,1).data.x, 1, "c3 is good");
		assert.equal(c0.table.at(1,1).data.y, 1, "c3 is good");

		assert.equal(c0.table.at(2,3).data.name, "c4", "c4 is good");
		assert.equal(c0.table.at(2,3).data.x, 2, "c4 is good");
		assert.equal(c0.table.at(2,3).data.y, 3, "c4 is good");

		assert.equal(c0.table.at(2,1).data.name, "c5", "c5 is good");
		assert.equal(c0.table.at(2,1).data.x, 2, "c5 is good");
		assert.equal(c0.table.at(2,1).data.y, 1, "c5 is good");


		var children = c0.get_children();
		assert.equal(children[0][0].name, "c3", "get_children -> name(0,0) is c3");
		assert.equal(children[0][0].x, 1, "get_children -> x(0,0) is 1");
		assert.equal(children[0][0].y, 1, "get_children -> y(0,0) is 1");
		assert.equal(children[0][1].name, "c5", "get_children -> name(0,1) is c5");
		assert.equal(children[0][1].x, 2, "get_children -> x(0,1) is 2");
		assert.equal(children[0][1].y, 1, "get_children -> y(0,1) is 1");
		assert.equal(children[1][0].name, "c2", "get_children -> name(0,1) is c2");
		assert.equal(children[1][0].x, 1, "get_children -> x(1,0) is 1");
		assert.equal(children[1][0].y, 2, "get_children -> y(1,0) is 2");
		assert.equal(children[1][1].name, "c1", "get_children -> name(0,1) is c1");
		assert.equal(children[1][1].x, 2, "get_children -> x(1,1) is 2");
		assert.equal(children[1][1].y, 2, "get_children -> y(1,1) is 2");
		assert.equal(children[2][0].name, '', "get_children -> name(0,1) is ''");
		assert.equal(children[2][0].x, 1, "get_children -> x(2,0) is 1");
		assert.equal(children[2][0].y, 3, "get_children -> y(2,0) is 3");
		assert.equal(children[2][1].name, 'c4', "get_children -> name(0,1) is c4");
		assert.equal(children[2][1].x, 2, "get_children -> x(2,1) is 2");
		assert.equal(children[2][1].y, 3, "get_children -> y(2,1) is 3");

		c0.remove_child(c1,"up");

		assert.equal(c0.get_row_count(), 3, "row count is 3");
		assert.equal(c0.get_col_count(), 2, "col count is 2");

		assert.propEqual(convert(c0.table.at(1,2).data), {name:"c2", x:1, y:2}, "c2 is good");
		assert.propEqual(convert(c0.table.at(1,1).data), {name:"c3", x:1, y:1}, "c3 is good");
		assert.propEqual(convert(c0.table.at(2,2).data), {name:"c4", x:2, y:2}, "c4 is good");
		assert.propEqual(convert(c0.table.at(2,1).data), {name:"c5", x:2, y:1}, "c5 is good");

		children = c0.get_children();
		assert.equal(children[0][0].name, "c3", "get_children -> name(0,0) is c3");
		assert.equal(children[0][0].x, 1, "get_children -> x(0,0) is 1");
		assert.equal(children[0][0].y, 1, "get_children -> y(0,0) is 1");
		assert.equal(children[0][1].name, "c5", "get_children -> name(0,1) is c5");
		assert.equal(children[0][1].x, 2, "get_children -> x(0,1) is 2");
		assert.equal(children[0][1].y, 1, "get_children -> y(0,1) is 1");
		assert.equal(children[1][0].name, "c2", "get_children -> name(0,1) is c2");
		assert.equal(children[1][0].x, 1, "get_children -> x(1,0) is 1");
		assert.equal(children[1][0].y, 2, "get_children -> y(1,0) is 2");
		assert.equal(children[1][1].name, 'c4', "get_children -> name(0,1) is c4");
		assert.equal(children[1][1].x, 2, "get_children -> x(1,1) is 2");
		assert.equal(children[1][1].y, 2, "get_children -> y(1,1) is 2");



		var c6 = factory.create_cell();
		c0.add_child_in_column(c6, 1);

		assert.propEqual(convert(c0.table.at(1,2).data), {name:"c2", x:1, y:2}, "c2 is good");
		assert.propEqual(convert(c0.table.at(1,1).data), {name:"c3", x:1, y:1}, "c3 is good");
		assert.propEqual(convert(c0.table.at(2,2).data), {name:"c4", x:2, y:2}, "c4 is good");
		assert.propEqual(convert(c0.table.at(2,1).data), {name:"c5", x:2, y:1}, "c5 is good");
		assert.propEqual(convert(c0.table.at(1,3).data), {name:"c6", x:1, y:3}, "c6 is good");
		assert.propEqual(convert(c0.table.at(2,3).data), {name:"", x:2, y:3}, "2,3 is good");
	});

	QUnit.test("Cell Test 3", function( assert ) {
		storage.clear();

		var c0 = factory.get_cell("c0");
		var c1 = factory.create_cell();
		c0.insert_child(c1, 1, 1, "down");

		var c2 = factory.create_cell();
		c0.insert_child(c2, 1, 1 , "right");

		var c3 = factory.create_cell();
		c0.insert_child(c3, 1, 1 , "down");

		var c4 = factory.create_cell();
		c0.insert_child(c4,2,2,"down");

		var c5 = factory.create_cell();
		c0.insert_child(c5,2,1,"down");

		c0.remove_child(c1,"up");

		var c6 = factory.create_cell();
		c0.add_child_in_column(c6, 1);

		assert.propEqual(convert(c0.table.at(1,2).data), {name:"c2", x:1, y:2}, "c2 is good");
		assert.propEqual(convert(c0.table.at(1,1).data), {name:"c3", x:1, y:1}, "c3 is good");
		assert.propEqual(convert(c0.table.at(2,2).data), {name:"c4", x:2, y:2}, "c4 is good");
		assert.propEqual(convert(c0.table.at(2,1).data), {name:"c5", x:2, y:1}, "c5 is good");
		assert.propEqual(convert(c0.table.at(1,3).data), {name:"c6", x:1, y:3}, "c6 is good");
		assert.propEqual(convert(c0.table.at(2,3).data), {name:"", x:2, y:3}, "2,3 is good");
	});


	QUnit.test("Cell Test 2", function( assert ) {
		var c0 = factory.get_cell("c0");
		var c1 = c0.insert_child(factory.create_cell(),1,1,"down");
		var c2 = c0.insert_child(factory.create_cell(),1,1,"down");
		var c3 = c0.insert_child(factory.create_cell(),1,1,"down");
		var c4 = c0.insert_child(factory.create_cell(),2,1,"down");
		var c5 = c0.insert_child(factory.create_cell(),2,1,"down");
		var c6 = c0.insert_child(factory.create_cell(),2,1,"down");

		assert.propEqual(convert(c0.get_child(1,3)), {name:"c1",x:1,y:3}, "c1 is good");
		assert.propEqual(convert(c0.get_child(1,2)), {name:"c2",x:1,y:2}, "c2 is good");
		assert.propEqual(convert(c0.get_child(1,1)), {name:"c3",x:1,y:1}, "c3 is good");
		assert.propEqual(convert(c0.get_child(2,3)), {name:"c4",x:2,y:3}, "c4 is good");
		assert.propEqual(convert(c0.get_child(2,2)), {name:"c5",x:2,y:2}, "c5 is good");
		assert.propEqual(convert(c0.get_child(2,1)), {name:"c6",x:2,y:1}, "c6 is good");

		c0.remove_child(c3, "up");

		assert.propEqual(convert(c0.get_child(1,2)), {name:"c1",x:1,y:2}, "c1 is good");
		assert.propEqual(convert(c0.get_child(1,1)), {name:"c2",x:1,y:1}, "c2 is good");
		assert.propEqual(convert(c0.get_child(2,3)), {name:"c4",x:2,y:3}, "c4 is good");
		assert.propEqual(convert(c0.get_child(2,2)), {name:"c5",x:2,y:2}, "c5 is good");
		assert.propEqual(convert(c0.get_child(2,1)), {name:"c6",x:2,y:1}, "c6 is good");

		c0.insert_child(c3, 2, 3, "down");

		assert.propEqual(convert(c0.get_child(1,2)), {name:"c1",x:1,y:2}, "c1 is good");
		assert.propEqual(convert(c0.get_child(1,1)), {name:"c2",x:1,y:1}, "c2 is good");
		assert.propEqual(convert(c0.get_child(2,4)), {name:"c4",x:2,y:4}, "c4 is good");
		assert.propEqual(convert(c0.get_child(2,2)), {name:"c5",x:2,y:2}, "c5 is good");
		assert.propEqual(convert(c0.get_child(2,1)), {name:"c6",x:2,y:1}, "c6 is good");
		assert.propEqual(convert(c0.get_child(2,3)), {name:"c3",x:2,y:3}, "c3 is good");
	});

	QUnit.test("test get_child", function( assert ) {
		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);

		assert.equal(c0.get_child(1,1).name, 'c1', "c1 is at 1,1");
		assert.equal(c0.get_child(1,1).x, 1, "c1 is at 1,1");
		assert.equal(c0.get_child(1,1).y, 1, "c1 is at 1,1");

		assert.equal(c0.get_child(1,2).name, "c2", "c2 is at 1,2");
		assert.equal(c0.get_child(1,2).x, 1, "c2 is at 1,2");
		assert.equal(c0.get_child(1,2).y, 2, "c2 is at 1,2");

		assert.equal(c0.get_child(2,2).name, "c3", "c3 is at 2,2");
		assert.equal(c0.get_child(2,2).x, 2, "c3 is at 2,2");
		assert.equal(c0.get_child(2,2).y, 2, "c3 is at 2,2");

		assert.equal(c0.get_child(2,1).name, "", "'' is at 2,2");
		assert.equal(c0.get_child(2,1).x, 2, "'' is at 2,1");
		assert.equal(c0.get_child(2,1).y, 1, "'' is at 2,1");
	});

	QUnit.test("test get_children", function( assert ) {
		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);

		var children = c0.get_children();

		assert.equal(children[0][0].name, 'c1', "c1 is at 1,1");
		assert.equal(children[0][0].x, 1, "c1 is at 1,1");
		assert.equal(children[0][0].y, 1, "c1 is at 1,1");

		assert.equal(children[1][0].name, "c2", "c2 is at 1,2");
		assert.equal(children[1][0].x, 1, "c2 is at 1,2");
		assert.equal(children[1][0].y, 2, "c2 is at 1,2");

		assert.equal(children[1][1].name, "c3", "c3 is at 2,2");
		assert.equal(children[1][1].x, 2, "c3 is at 2,2");
		assert.equal(children[1][1].y, 2, "c3 is at 2,2");

		assert.equal(children[0][1].name, "", "'' is at 2,2");
		assert.equal(children[0][1].x, 2, "'' is at 2,1");
		assert.equal(children[0][1].y, 1, "'' is at 2,1");
	});

	QUnit.test("test get_data bug 1", function( assert ) {
		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);
		c0 = new CellModel(c0.get_data());


		var children = c0.get_children();
		assert.equal(children[0][0].name, 'c1', "c1 is at 1,1");
		assert.equal(children[0][0].x, 1, "c1 is at 1,1");
		assert.equal(children[0][0].y, 1, "c1 is at 1,1");

		assert.equal(children[1][0].name, "c2", "c2 is at 1,2");
		assert.equal(children[1][0].x, 1, "c2 is at 1,2");
		assert.equal(children[1][0].y, 2, "c2 is at 1,2");

		assert.equal(children[1][1].name, "c3", "c3 is at 2,2");
		assert.equal(children[1][1].x, 2, "c3 is at 2,2");
		assert.equal(children[1][1].y, 2, "c3 is at 2,2");

		assert.equal(children[0][1].name, "", "'' is at 2,2");
		assert.equal(children[0][1].x, 2, "'' is at 2,1");
		assert.equal(children[0][1].y, 1, "'' is at 2,1");
	});

	QUnit.test("test get_parent", function( assert ) {
		var data = {children:[["c1"],["c2","c3"]]};

		var c0 = new CellModel(data);
		c0 = new CellModel(c0.get_data());
		assert.equal(c0.get_parent(), null, "c0.get_parent is null");

		var c1 = c0.get_child(1,1);
		assert.ok(c1.get_parent(), "c1.get_parent() exists");
		assert.equal(c1.get_parent(), c0, "c0 is the c1's father")
	});

	QUnit.test("insert_child", function( assert ) {
		var c0 = new CellModel("c0");
		var c1 = new CellModel("c1");

		c0.insert_child(c1,1,1,"right");

		assert.equal(c1.get_parent(), c0, "get_parent -> c0 is c1's father");
		assert.equal(c1.x, 1, "c1.x = 1");
		assert.equal(c1.y, 1, "c1.y = 1");
	});

	QUnit.test("save 1", function( assert ) {
		storage.clear();
		var c0 = new CellModel("c0",null,1,1,factory);
		c0.save();

		var data = JSON.parse(storage["c0"]);
		assert.equal(data.name, "c0", "data -> name is c0");
		assert.equal(data.content, "", "data -> content is ''");

		c0.data.content = "coucou";
		c0.save();

		var data = JSON.parse(storage["c0"]);
		assert.equal(data.name, "c0", "data -> name is c0");
		assert.equal(data.content, "coucou", "data -> content is 'coucou'");
	});

	QUnit.test("save 2", function( assert ) {
		storage.clear();
		var c1 = new CellModel('', null,1,1,factory);
		assert.equal(c1.get_name(), "", "c1 -> name is ''");
		c1.save()
		assert.equal(c1.get_name(), 'c1', "c1 -> name is 'c1'");
	});

	QUnit.test("save 3", function( assert ) {
		storage.clear();
		var data = {name:"c0", children:[["c4"],["c2","c3"]]};

		var c0 = new CellModel(data,null,1,1,factory);
		var c1 = c0.get_child(2,1);

		assert.equal(c1.get_name(), "", "c1 -> no name");
		c1.save();
		assert.equal(c1.get_name(), "c1", "c1 -> name is 'c1'");

		var c1bis = c0.get_child(2,1);
		assert.equal(c1bis.get_name(), "c1", "c1bis -> name is 'c1'");
	});

	QUnit.test("get_data", function( assert ) {
		storage.clear();
		var data = {name:"c0", children:[["c4"],["c2","c3"]]};

		var c0 = new CellModel(data,null,1,1,factory);
		var c1 = c0.get_child(2,1);

		assert.equal(c1.get_name(), "", "c1 -> no name");
		c1.save();
		assert.equal(c1.get_name(), "c1", "c1 -> name is 'c1'");

		assert.deepEqual(c0.get_data(),{archived:false,name:"c0", children:[["c4","c1"],["c2","c3"]],col_count: 2,content: "",layout: 3,name: "c0",row_count:2});
		var data = JSON.parse(storage["c0"]);
		assert.deepEqual(data,{archived:false,name:"c0", children:[["c4","c1"],["c2","c3"]],col_count: 2,content: "",layout: 3,name: "c0",row_count:2});
	});

	QUnit.test("Bug", function( assert ) {
		storage.clear();

		var c0 = factory.get_cell("c0");
		var c1 = factory.create_cell();
		c0.insert_child(c1, 1, 1, "down");
		assert.equal(c0.table.at(1,1).data.get_name(), "c1");

		var c2 = factory.create_cell();
		c0.insert_child(c2, 1, 1 , "right");
		assert.equal(c0.table.at(2,1).data.get_name(), "c1");
		assert.equal(c0.table.at(1,1).data.get_name(), "c2");

		var c3 = factory.create_cell();
		c0.insert_child(c3, 1, 1 , "down");

		assert.equal(c0.table.at(1,1).data.get_name(), "c3");
		assert.equal(c0.table.at(2,1).data.get_name(), "c1");
		assert.equal(c0.table.at(1,2).data.get_name(), "c2");
		assert.equal(c0.table.at(2,2).data.get_name(), "");


		var c4 = factory.create_cell();
		console.log("here");
		c0.insert_child(c4,2,2,"down");

		assert.equal(c0.table.at(1,1).data.get_name(), "c3");
		assert.equal(c0.table.at(2,1).data.get_name(), "c1");
		assert.equal(c0.table.at(1,2).data.get_name(), "c2");
		assert.equal(c0.table.at(2,2).data.get_name(), "c4");

		assert.equal(c0.get_row_count(), 2, "row count is 2");
		assert.equal(c0.get_col_count(), 2, "col count is 2");
	});

	return {}
})