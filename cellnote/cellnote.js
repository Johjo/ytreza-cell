include('cellnote.cellnote',['cellnote.controller.cell', 'cellnote.view.main_view','cellnote.models.localefactory'], function(Controller, MainView, ModelsManagerLocale) {
	var s, factory, ihm,
	CellNote = {

		settings: {
			default_cell : "c0",
			main_div : "main"
		},

		get_param : function(param_name) {
			var url = window.location.search;
			var param_value = (RegExp(param_name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
			if (param_value) {
				param_value = decodeURI(param_value);
			}

			return param_value;
		},

		init : function() {
			s = this.settings;

			var cell_name = this.get_param("cell") || s.default_cell;

			var controller = new Controller();
			controller.set_model_manager(new ModelsManagerLocale());

			var view = new MainView(controller, cell_name, document.getElementById(s.main_div));
			view.init();
		}
	};

	return CellNote;
})

