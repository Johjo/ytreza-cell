var ihm;
module('ihm.js',{
    setup: function() {
    	ihm = new Ihm();
    }
});

function MockCellView01(ihm) {
	this.selected = false;
	this.ihm = ihm;
}

MockCellView01.prototype.on_select = function() {
	this.selected = true;
	this.ihm.on_select(this);
};

MockCellView01.prototype.on_deselect = function() {
	this.selected = false;
};

QUnit.test("selection d'une cellule et déselection des autres", function( assert ) {
	var cv1 = new MockCellView01(ihm);
	var cv2 = new MockCellView01(ihm);
	var cv3 = new MockCellView01(ihm);

	assert.equal(cv1.selected,false);
	assert.equal(cv2.selected,false);
	assert.equal(cv3.selected,false);

	cv1.on_select();
	assert.equal(cv1.selected,true);
	assert.equal(cv2.selected,false);
	assert.equal(cv3.selected,false);

	cv2.on_select();
	assert.equal(cv1.selected,false);
	assert.equal(cv2.selected,true);
	assert.equal(cv3.selected,false);

	cv3.on_select();
	assert.equal(cv1.selected,false);
	assert.equal(cv2.selected,false);
	assert.equal(cv3.selected,true);
});