include('tools.htmltools', function() {
	function create_html(element,class_name) {
		var div = document.createElement(element);
		if (class_name!="") {
			div.className = class_name;
		}
		return div;
	}


	function html_to_zen_coding(element) {
		var text = element.nodeName;
		if (!element.id == "") {
			text+="#"+element.id;
		}

		if (!element.className == "") {
			text+="."+element.className;
		}

		var child = "";
		var at_least_one_child = false;
		for (i in element.childNodes) {
			if (element.childNodes[i].nodeType == 1) {
				if (at_least_one_child) {child+="&";}
				at_least_one_child = true;
				child+=html_to_zen_coding(element.childNodes[i]);
			}
		}

		if (at_least_one_child) {text+=">("+child+")";}

		return text;
	}

	function delete_children(element) {
		while (element.firstChild) element.removeChild(element.firstChild);
	}


	return {
		create_html: create_html,
		html_to_zen_coding : html_to_zen_coding,
		delete_children, delete_children
	}
})