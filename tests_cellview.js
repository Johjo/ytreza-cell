var factory;

function TestFactoryBis() {
	this.last_cell = 0;
	this.cells = {};
}

TestFactoryBis.prototype.create = function() {
	this.last_cell = this.last_cell + 1;
	data = {name:"c" + this.last_cell, content:""};

	var cell = new Cell(data, this);
	this.cells[cell.get_name()] = cell;
	return cell;
};

TestFactoryBis.prototype.get = function(name) {
	return this.cells[name];
};

function IhmTest() {
	BaseView.call(this,this,0);
}

IhmTest.prototype = new BaseView;

IhmTest.prototype.get_ihm = function() {
	return this;
};

IhmTest.prototype.register = function(cellview) {
};

IhmTest.prototype.get_level = function() {
	return 0;
};

IhmTest.prototype.get_max_level = function() {
	return 6;
};

var ihm_test = new IhmTest();


module('Class CellController',{
    setup: function() {
        factory = new TestFactoryBis();
    }
});


QUnit.test("new CellController() 1", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);

	assert.equal(c1.get_name(), "c1", "c1.get_name() = c1");

	assert.equal(cv1.get_html_zen_coding(),'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))');
});

QUnit.test("new CellController() 2", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);
	var cv2 = new CellController(null,cv1,1,1);
	
	assert.ok(cv2);
	assert.equal(cv2.get_content(),"");

	assert.equal(cv1.get_html_zen_coding(),'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))');
	assert.equal(cv2.get_html_zen_coding(),'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>(DIV.new_child>(DIV>(BUTTON))))');
});

QUnit.test("get child if child doesn't exist", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);
	
	assert.ok(! cv1.has_child(1,1));

	var cv2 = cv1.get_child(1,1);
	assert.ok(cv2==null);
});

QUnit.test("load child", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var c2 = c1.create_child(1,1);
	c2.set_content("c2");
	var c3 = c1.create_child(1,2);
	c3.set_content("c3");

	var cv1 = new CellController(c1,ihm_test);
	
	assert.ok(cv1.has_child(1,1),"cv1 a un enfant en (1,1)");
	assert.ok(cv1.has_child(1,2),"cv1 a un enfant en (1,2)");
	assert.ok(!cv1.has_child(2,1),"cv1 n'a pas d'enfant en (2,1)");
	assert.ok(!cv1.has_child(2,2),"cv1 n'a pas d'enfant en (2,2)");

	assert.ok(c1.has_child(1,1),"c1 a un enfant en (1,1)");
	assert.ok(c1.has_child(1,2),"c1 a un enfant en (1,2)");
	assert.ok(!c1.has_child(2,1),"c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2),"c1 n'a pas d'enfant en (2,2)");



	assert.equal(cv1.get_child(1,1).get_cell().get_content(),"c2", "c2 existe bien");
	assert.equal(cv1.get_child(1,2).get_cell().get_content(),"c3", "c3 existe bien");

	assert.equal(cv1.get_html_zen_coding(),'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.row>(DIV>(DIV.row>(DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))))&DIV.new_child>(DIV>(BUTTON)))&DIV.row>(DIV.new_child>(DIV>(BUTTON))&DIV.new_child>(DIV>(BUTTON)))))')

	c1.create_child(3,3);

	assert.equal(cv1.get_html_zen_coding(),
		'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.row>(DIV>(DIV.row>(DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON)))))&DIV.row>(DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON)))))&DIV.row>(DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))&DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))))&DIV.new_child>(DIV>(BUTTON)))&DIV.row>(DIV.new_child>(DIV>(BUTTON))&DIV.new_child>(DIV>(BUTTON)))))')
});

QUnit.test("bug cellview crée des enfants à cell dans le dos", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();

	assert.ok(!c1.has_child(1,1),"c1 n'a pas d'enfant en (1,1)");
	assert.ok(!c1.has_child(1,2),"c1 n'a pas d'enfant en (1,2)");
	assert.ok(!c1.has_child(2,1),"c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2),"c1 n'a pas d'enfant en (2,2)");

	c1.create_child(1,1);
	assert.ok(c1.has_child(1,1),"c1 a un enfant en (1,1)");
	assert.ok(!c1.has_child(1,2),"c1 n'a pas d'enfant en (1,2)");
	assert.ok(!c1.has_child(2,1),"c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2),"c1 n'a pas d'enfant en (2,2)");

	c1.create_child(1,2);
	assert.ok(c1.has_child(1,1),"c1 a un enfant en (1,1)");
	assert.ok(c1.has_child(1,2),"c1 a un enfant en (1,2)");
	assert.ok(!c1.has_child(2,1),"c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2),"c1 n'a pas d'enfant en (2,2)");

	var cv1 = new CellController(c1,ihm_test);
	assert.ok(c1.has_child(1,1),"c1 a un enfant en (1,1)");
	assert.ok(c1.has_child(1,2),"c1 a un enfant en (1,2)");
	assert.ok(!c1.has_child(2,1),"c1 n'a pas d'enfant en (2,1)");
	assert.ok(!c1.has_child(2,2),"c1 n'a pas d'enfant en (2,2)");
});



QUnit.test("CellController.select()", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);

	// todo refaire ce test
});

QUnit.test("CellController.get_cell()", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);

	assert.ok(cv1.get_cell());
});

QUnit.test("CellController.create_child()", function( assert ) {
	var factory = new TestFactoryBis();
	var c1 = factory.create();
	var cv1 = new CellController(c1,ihm_test);

	var cv2 = cv1.create_child(1,1);
	assert.ok(cv2);
	assert.equal(cv1.get_row_count(),1);
	assert.equal(cv1.get_col_count(),1);

	assert.throws(function() {cv1.create_child(1,1);},CellViewException);

	assert.equal(cv1.get_html_zen_coding(),
		'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.row>(DIV>(DIV.row>(DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))))&DIV.new_child>(DIV>(BUTTON)))&DIV.row>\
(DIV.new_child>(DIV>(BUTTON))&DIV.new_child>(DIV>(BUTTON)))))');
	assert.equal(cv2.get_html_zen_coding(),
		'DIV.cell>(DIV.content&DIV.action&DIV.see&DIV.children>\
(DIV.new_child>(DIV>(BUTTON))))');
});

module('Behaviour',{
    setup: function() {
    }
});

QUnit.test("behaviour", function( assert ) {
	var a = 10;
	var b = new BehaviourDisplay(a);

	assert.equal(b.can_display(), false);
	assert.equal(b.can_edit(), true);
	assert.equal(b.can_mouse_over(), true);


	assert.equal(b.cellview,10);

});

